<?php

namespace App\Repository;
use App\Models\Role;
use App\Repository\Interfaces\RoleRepository;
class EloquentRoleRepository implements RoleRepository
{
    private $models;
    public function __construct(Role $models)
    {
        $this->models = $models;
    }

    public function data(){
        $response = $this->models->get()->toArray();
        return $response;
    }
      
      
    
}