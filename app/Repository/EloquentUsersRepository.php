<?php

namespace App\Repository;

use App\Models\User;
use App\Repository\Interfaces\UsersRepository;
use Illuminate\Support\Arr;
class EloquentUsersRepository implements UsersRepository
{
    private $models;
    public function __construct(User $models)
    {
        $this->models = $models;
    }

    public function data(){
        $response = $this->models->with('roles')->get();
        return $response;
    }

    public function store($data){
          // Create New User
          $user = new User();
          $user->name = $data['name'];
          $user->email = $data['email'];
          $user->password = bcrypt($data['password']);
          $user->save();
          $user->syncRoles($data['roles']);
          if($user!= null){
            $response = [
                'success' => true,
                'message' => 'User added successfully'
            ];
          }else{
            $response = [
                'success' => false,
                'message' => 'User added motsuccessfully'
            ];
          }
  
        return $response;
    }
      
      
    
}