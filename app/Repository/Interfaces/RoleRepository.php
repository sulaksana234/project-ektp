<?php

namespace App\Repository\Interfaces;

interface RoleRepository
{
    public function data();
}