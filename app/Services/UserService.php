<?php

namespace App\Services;
use App\Repository\Interfaces\UsersRepository;

class UserService
{
    private $userRepository;

    public function __construct(UsersRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function data()
	{
		return $this->userRepository->data();
	}

    public function store($data){
        return $this->userRepository->store($data);
    }

   
}
