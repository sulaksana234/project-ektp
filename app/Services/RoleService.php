<?php

namespace App\Services;
use App\Repository\Interfaces\RoleRepository;

class RoleService
{
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function data()
	{
		return $this->roleRepository->data();
	}

   
}
