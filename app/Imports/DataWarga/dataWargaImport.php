<?php

namespace App\Imports\DataWarga;

use App\Models\DataWarga;
use App\Models\Post;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
class dataWargaImport implements ToModel
{
  



    public function model(array $row)
    {   

        // trim the spaces from each value in the row
        return new DataWarga([
            'nama_lengkap' => $row[2],
            'nama_panggilan' => $row[2],
            'nik' => $row[1],
            'jenis_kelamin' => $row[3],
            'tempat_lahir' => $row[4],
            'tanggal_lahir' => $row[5],
            'agama' => $row[7],
            'no_kk' => $row[0],
            'pendidikan' => 'sd',
            'jenis_pekerjaan' => 'ibu_rumah_tangga',
            'golongan_darah' => $row[6],
            'status_perkawinan' => 'belum_kawin',
            'nik_ibu' => $row[9]                                                                                ,
            'nama_lengkap_ibu' => $row[10]                                                                                ,
            'nik_ayah' => $row[11]                                                                                ,
            'nama_lengkap_ayah' => $row[12]                                                                                ,
            'alamat_domisili' => $row[13]                                                                                ,
            'alamat_dusun' => $row[14]                                                                                ,
            'status_ektp' => 'no_ektp'                                                                               ,

        ]);
    }
}