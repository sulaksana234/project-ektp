<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Role;
use DataTables;

class RoleController extends Controller
{
    //
    public function index(){
        // if (!auth()->user()->can('roles.index')) {
        //     abort(403, 'Unauthorized action.');
        // }
        $permissions = Permission::all();
        return view('roles.index',compact('permissions'));
    }

    public function data(){
        $data = Role::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="editRoles('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" onclick="deleteRoles('.$row->id.')" class="btn btn-danger btn-sm deletePost"><i class="fa fa-trash-o"></i></a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function store(Request $request,Role $role){
        try {
            //code...
            $validated = $request->validate([
            'name' => 'required|unique:permissions,name,'. $request->id .'',

     
                'display_name' => ['required'],
                'description' => ['required'],
            ]);
            $role = Role::updateOrCreate(['id' => $request->id],
            [
                'name' => $request->name, 
                'display_name' => $request->display_name, 
                'description' => $request->description
            ]);        
            $role->syncPermissions($request->permissions); // parameter can be a Permission object, array or id

            return response()->json([
                'success'=> true,
                'message' => 'Roles saved successfully.',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success'=> false,
                'message' => $th->getMessage(),
            ]);
        }
      
    }
    public function edit($id)
    {
        $permissions = Role::with('permissions')->find($id);
        return response()->json($permissions);
    }

    public function delete($id){
        Role::where('id', $id)->delete();

        //return response
        return response()->json([
            'success' => true,
            'message' => 'Data Roles Berhasil Dihapus!.',
        ]); 
    }
}
