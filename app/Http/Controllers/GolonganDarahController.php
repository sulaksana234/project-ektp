<?php

namespace App\Http\Controllers;

use App\Models\GolonganDarah;
use Illuminate\Http\Request;

class GolonganDarahController extends Controller
{
    //
    public function dataAll(){
        $golonganDarah = GolonganDarah::all();
        return $golonganDarah;
    }
}
