<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DataTables;
use Laravolt\Indonesia\Models\Province;

class ProvinsiController extends Controller
{
    //
    public function index(){
        return view('provinsi.index');
    }

    public function data(){
        $data = Province::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="editProvinsi('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" onclick="deleteProvinsi('.$row->id.')" class="btn btn-danger btn-sm deletePost"><i class="fa fa-trash-o"></i></a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);

    }
    public function store(Request $request){
        try {
            //code...
            $validated = $request->validate([
                'code' => 'required|unique:indonesia_provinces,code,'. $request->id .'',
                'name' => ['required'],
            ]);
            Province::updateOrCreate(['id' => $request->id],
            [
                'code' => $request->code, 
                'name' => $request->name, 
            ]);        
    
            return response()->json([
                'success'=> true,
                'message' => 'Provinsi saved successfully.',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success'=> false,
                'message' => $th->getMessage(),
            ]);
        }
      
    }

    public function edit($id)
    {
        $provinsi = Province::find($id);
        return response()->json($provinsi);
    }

    public function delete($id){
        Province::where('id', $id)->delete();

        //return response
        return response()->json([
            'success' => true,
            'message' => 'Data Provinsi Berhasil Dihapus!.',
        ]); 
    }
}
