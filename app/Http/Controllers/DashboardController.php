<?php

namespace App\Http\Controllers;

use App\Models\DataWarga;
use Illuminate\Http\Request;
use DataTables;

class DashboardController extends Controller
{
    //
    public function index(){
        $dataJumlahEktp = [];
    
        $dataJumlahPengajuanEktp = [];
        $dataJumlahBelumMempunyaiEktp = [];
        $jumlahWarga = DataWarga::count();
        $jumlahWargaPria = DataWarga::where('jenis_kelamin','laki_laki')->where('banjar','!=','ling_kertha_pascima')->count();
        $jumlahWargaPriaPendatang = DataWarga::where('jenis_kelamin','laki_laki')->where('banjar','=','ling_kertha_pascima')->count();
        $jumlahWargaWanita = DataWarga::where('jenis_kelamin','perempuan')->where('banjar','!=','ling_kertha_pascima')->count();
        $jumlahWargaWanitaPendatang = DataWarga::where('jenis_kelamin','perempuan')->where('banjar','=','ling_kertha_pascima')->count();
        $jumlahKK = DataWarga::select('no_kk')->groupBy('no_kk')->get();
        $jumlahKKGegem = DataWarga::select('no_kk','banjar')->where('banjar','!=','ling_kertha_pascima')->groupBy('no_kk','banjar')->get();
        $jumlahKKGegemYudistira = DataWarga::select('no_kk','banjar')->where('banjar','!=','ling_kertha_pascima')->where('tempekan','yudistira')->groupBy('no_kk','banjar')->get();
        $jumlahKKGegemArjuna = DataWarga::select('no_kk','banjar')->where('banjar','!=','ling_kertha_pascima')->where('tempekan','arjuna')->groupBy('no_kk','banjar')->get();
        $jumlahKKGegemBima = DataWarga::select('no_kk','banjar')->where('banjar','!=','ling_kertha_pascima')->where('tempekan','bima')->groupBy('no_kk','banjar')->get();
        $jumlahKKGegemNakulaSahadewa = DataWarga::select('no_kk','banjar')->where('banjar','!=','ling_kertha_pascima')->where('tempekan','nakula&sahadewa')->groupBy('no_kk','banjar')->get();

        $jumlahKKPendatang = DataWarga::select('no_kk','banjar')->where('banjar','=','ling_kertha_pascima')->groupBy('no_kk','banjar')->get();


        $dataEktp = DataWarga::all();
        foreach ($dataEktp as $key => $value) {
            # code...
            $birthDate = new \DateTime($value->tanggal_lahir);
            $today = new \DateTime("today");
            if ($birthDate > $today) {
                return "0 tahun 0 bulan 0 hari";
            }
            $y = $today->diff($birthDate)->y;
            if($y>=17 && ($value->status_ektp == 'done')){
                array_push($dataJumlahEktp,$value);
            }
        }
        $dataEktp = DataWarga::all();

        foreach ($dataEktp as $key => $value) {
            # code...
            $birthDate = new \DateTime($value->tanggal_lahir);
            $today = new \DateTime("today");
            if ($birthDate > $today) {
                return "0 tahun 0 bulan 0 hari";
            }
            $y = $today->diff($birthDate)->y;
            if($y>=17 && ($value->status_ektp == 'pengajuan')){
                array_push($dataJumlahPengajuanEktp,$value);
            }
        }
        $dataEktp = DataWarga::all();


        foreach ($dataEktp as $key => $value) {
            # code...
            $birthDate = new \DateTime($value->tanggal_lahir);
            $today = new \DateTime("today");
            if ($birthDate > $today) {
                return "0 tahun 0 bulan 0 hari";
            }
            $y = $today->diff($birthDate)->y;
            if($y>=17 && ($value->status_ektp == 'no_ektp')){
                array_push($dataJumlahBelumMempunyaiEktp,$value);
            }
        }

        $persentasiJumlahPemilikEktp=round(count($dataJumlahEktp)/$jumlahWarga * 100,2);
        $persentasiJumlahPengajuanEktp=round(count($dataJumlahPengajuanEktp)/$jumlahWarga * 100,2);
        $persentasiJumlahBelumMempunyaiEktp=round(count($dataJumlahBelumMempunyaiEktp)/$jumlahWarga * 100,2);

        return view('dashboard.index',compact(
            'jumlahWarga',
            'jumlahWargaPria',
            'jumlahWargaPriaPendatang',
            'jumlahWargaWanita',
            'jumlahWargaWanitaPendatang',
            'persentasiJumlahPemilikEktp',
            'persentasiJumlahPengajuanEktp',
            'persentasiJumlahBelumMempunyaiEktp',
            'jumlahKK',
            'jumlahKKGegem',
            'jumlahKKGegemYudistira',
            'jumlahKKGegemArjuna',
            'jumlahKKGegemBima',
            'jumlahKKGegemNakulaSahadewa',
            'jumlahKKPendatang',
        ));
    }

    public function dataektp(){
        $data = [];
        try {
            //code...
            $dataEktp = DataWarga::all();
            foreach ($dataEktp as $key => $value) {
                # code...
                $birthDate = new \DateTime($value->tanggal_lahir);
                $today = new \DateTime("today");
                if ($birthDate > $today) {
                    return "0 tahun 0 bulan 0 hari";
                }
                $y = $today->diff($birthDate)->y;
                if($y>=17 && ($value->status_ektp == 'no_ektp')){
                    array_push($data,$value);
                }
            }
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){

                   $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="detailWarga('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';


                    return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        } catch (\Throwable $th) {
            //throw $th;
            dd($th->getMessage());

        }
    }
}
