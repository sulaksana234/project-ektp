<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
Use App\Services\UserService;
use App\Services\Support\RoleService;
use DB;
use DataTables;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UsersController extends Controller
{
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index(){
        $role = Role::all();
        return view('users.index',compact('role'));
    }

  

    public function data(Request $request){
        try {
            //code...
                $data = $this->service->data();
                return DataTables::of($data)
                        ->addIndexColumn()
                        ->addColumn('roles', function (User $user) {
                           return $user->roles->map(function($user) {

                                return $user->display_name;
                            })->implode('<br>');
                        })

                        
                        ->addColumn('action', function($row){
       
                               $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct" onclick="editUser('.$row->id.')"><i class="fa fa-edit"></i><code data-intro="Tambah Users" data-step="2" data-hint="Button Untuk Tambah User">Edit User</code></a>';
       
                               $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct" onclick="deleteUser('.$row->id.')"><i class="fa fa-trash"></i> Delete User</a>';
        
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);

            

        } catch (\Throwable $th) {
            //throw $th;
            dd($th->getMessage());
        }
      
    }

    public function store(Request $request){

        try {
            //code...
            if($request->id == ''){
                $request->validate([
                    'name' => 'required|max:50',
                    'email' => 'required|max:100|email|unique:users',
                    'password' => 'required|min:6',
                ]);
                   
    
                $data = [
                        'name' => $request->name,
                        'email' => $request->email,
                        'password' => Hash::make($request->password),
                        'roles' => $request->roles,
                ];
                $response = $this->service->store($data);
                return response()->json(
                    [
                        'success'=> true,
                        'message'=> 'User berhasil ditambahkan'
                ]);
            }else{
                $request->validate([
                    'name' => 'required|max:50',
                    'email' => 'required|unique:users,email,'. $request->id .'',

                ]);
                   
    
                $data = [
                        'name' => $request->name,
                        'email' => $request->email,
                        'password' => Hash::make($request->password),
                        'roles' => $request->roles,
                ];
                $user = User::updateOrCreate(['id' => $request->id],
                $data);    
                return response()->json(
                    [
                        'success'=> true,
                        'message'=> 'User berhasil diupdate'
                    ]);
            }
        
        } catch (\Throwable $th) {
            //throw $th;
            dd($th->getMessage());
        }
  
    }

    public function edit($id){
        $data = User::with('roles')->find($id);
        return $data;
    }

    public function delete($id){
        $data = User::with('roles')->find($id)->delete();
        return response()->json(
            [
                'success'=> true,
                'message'=> 'User berhasil dihapus'
            ]);
    }
}