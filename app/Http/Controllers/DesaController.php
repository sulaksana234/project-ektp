<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravolt\Indonesia\Models\Kecamatan;
use Laravolt\Indonesia\Models\Kelurahan;
use DataTables;

class DesaController extends Controller
{
    //
    public function index(){
        $kecamatan = Kecamatan::latest()->get();
        return view('kelurahan.index',compact('kecamatan'));
    }

    public function data(){
        $data = Kelurahan::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="editKelurahan('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" onclick="deleteKelurahan('.$row->id.')" class="btn btn-danger btn-sm deletePost"><i class="fa fa-trash-o"></i></a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);

    }

    public function store(Request $request){
        try {
            //code...
            $validated = $request->validate([
                'code' => 'required|unique:indonesia_villages,code,'. $request->id .'',
                'name' => ['required'],
            ]);
            Kelurahan::updateOrCreate(['id' => $request->id],
            [
                'district_code' => $request->district_code,
                'code' => $request->code, 
                'name' => $request->name, 
            ]);        
    
            return response()->json([
                'success'=> true,
                'message' => 'Desa saved successfully.',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success'=> false,
                'message' => $th->getMessage(),
            ]);
        }
      
    }

    public function edit($id){
        $kelurahan = Kelurahan::find($id);
        return response()->json($kelurahan);
    }

    public function delete($id){
        Kelurahan::where('id', $id)->delete();

        //return response
        return response()->json([
            'success' => true,
            'message' => 'Data Kelurahan Berhasil Dihapus!.',
        ]); 
    }
}
