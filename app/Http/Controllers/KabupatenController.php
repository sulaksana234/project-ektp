<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravolt\Indonesia\Models\Cities;
use Laravolt\Indonesia\Models\Kabupaten;
use DataTables;
use Laravolt\Indonesia\Models\Province;

class KabupatenController extends Controller
{
    //
    public function index(){
        $provinsi = Province::latest()->get();
        return view('kabupaten.index',compact('provinsi'));
    }

    public function data(){
        $data = Kabupaten::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="editProvinsi('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" onclick="deleteProvinsi('.$row->id.')" class="btn btn-danger btn-sm deletePost"><i class="fa fa-trash-o"></i></a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);

    }

    public function store(Request $request){
        try {
            //code...
            $validated = $request->validate([
                'code' => 'required|unique:indonesia_cities,code,'. $request->id .'',
                'name' => ['required'],
            ]);
            Kabupaten::updateOrCreate(['id' => $request->id],
            [
                'province_code' => $request->province_code,
                'code' => $request->code, 
                'name' => $request->name, 
            ]);        
    
            return response()->json([
                'success'=> true,
                'message' => 'Kabupaten saved successfully.',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success'=> false,
                'message' => $th->getMessage(),
            ]);
        }
      
    }

    public function edit($id){
        $kabupaten = Kabupaten::find($id);
        return response()->json($kabupaten);
    }

    public function delete($id){
        Kabupaten::where('id', $id)->delete();

        //return response
        return response()->json([
            'success' => true,
            'message' => 'Data Kabupaten Berhasil Dihapus!.',
        ]); 
    }
}
