<?php

namespace App\Http\Controllers;

use App\Models\Agama;
use Illuminate\Http\Request;

class AgamaController extends Controller
{
    //
    public function dataAll(){
        $agama = Agama::all();
        return $agama;
    }
}
