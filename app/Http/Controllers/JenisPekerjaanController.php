<?php

namespace App\Http\Controllers;

use App\Models\Pekerjaan;
use Illuminate\Http\Request;
use DataTables;

class JenisPekerjaanController extends Controller
{
    public function index(){
        return view('jenis-pekerjaan.index');
    }

    public function data(){
        $data = Pekerjaan::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="editJenisPekerjaan('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" onclick="deleteJenisPekerjaan('.$row->id.')" class="btn btn-danger btn-sm deletePost"><i class="fa fa-trash-o"></i></a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    //
    public function dataAll(){
        $jenisPekerjaan = Pekerjaan::all();
        return $jenisPekerjaan;
    }
    
    public function store(Request $request){
        try {
            //code...
            $validated = $request->validate([
                'name' => 'required|unique:jenis_pekerjaan,name,'. $request->id .'',
                'display_name' => ['required'],
            ]);
            Pekerjaan::updateOrCreate(['id' => $request->id],
            [
                'name' => $request->name, 
                'display_name' => $request->display_name, 
            ]);        
    
            return response()->json([
                'success'=> true,
                'message' => 'Jenis Pekerjaan saved successfully.',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success'=> false,
                'message' => $th->getMessage(),
            ]);
        }
      
    }

    public function edit($id){
        $jenisPekerjaan = Pekerjaan::find($id);
        return response()->json($jenisPekerjaan);
    }

    public function delete($id){
        Pekerjaan::where('id', $id)->delete();

        //return response
        return response()->json([
            'success' => true,
            'message' => 'Data Jenis Pekerjaan Berhasil Dihapus!.',
        ]); 
    }


}
