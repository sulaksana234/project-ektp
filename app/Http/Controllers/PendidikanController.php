<?php

namespace App\Http\Controllers;

use App\Models\Pendidikan;
use Illuminate\Http\Request;

class PendidikanController extends Controller
{
    //
    public function dataAll(){
        $pendidikan = Pendidikan::all();
        return $pendidikan;
    }
}
