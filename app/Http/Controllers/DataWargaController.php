<?php

namespace App\Http\Controllers;

use App\Imports\DataWarga\dataWargaImport;
use App\Models\Agama;
use App\Models\DataWarga;
use App\Models\GolonganDarah;
use App\Models\Pekerjaan;
use App\Models\Pendidikan;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DataWargaController extends Controller
{
    //
    public function index(){
        $pendidikan = Pendidikan::all();
        $pekerjaan = Pekerjaan::all();
        $agama = Agama::all();
        $golonganDarah = GolonganDarah::all();
        return view('dataWarga.index',compact('pendidikan','agama','pekerjaan','golonganDarah'));
    }

    public function data(){
        $data = DataWarga::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="detailWarga('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';
                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Detail" onclick="detailWarga('.$row->id.')" class="btn btn-danger btn-sm detailWarga"><i class="fa fa-info-circle"></i></a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" onclick="deleteWarga('.$row->id.')" class="btn btn-danger btn-sm deletePost"><i class="fa fa-trash-o"></i></a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function importDataWarga(Request $request){

       	// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file_siswa',$nama_file);
 
		// import data
		Excel::import(new dataWargaImport, public_path('/file_siswa/'.$nama_file));
    }

    public function store(Request $request){
        DB::beginTransaction();
        try {
            //code...
            $validator = \Validator::make($request->all(), [
                'nama_lengkap' => 'required',
                'no_kk' => 'required|string|min:16|max:16',
                'nik' => 'required|string|min:16|max:16|unique:data_warga,nik,'. $request->id .'',
                'jenis_kelamin' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'agama' => 'required',
                'status_perkawinan' => 'required',
                'status_didalam_keluarga' => 'required',
                'kewarganegaraan' => 'required',
                // 'status_ektp' => 'required',
                // 'nik_ayah' => 'required|string|min:16|max:16',
                // 'nama_lengkap_ayah' => 'required',
                // 'nik_ibu' => 'required|string|min:16|max:16',
                // 'nama_lengkap_ibu' => 'required',
                // 'alamat_asal' => 'required',
                // 'alamat_domisili' => 'required',


            ]);
            
            if ($validator->fails())
            {
                return response()->json(
                    [   
                        'success' => false,
                        'title' => 'Gagal',
                        'message'=>$validator->errors()->all()
                    ]
                );
            }
            if($request->id == ''){
                $dataWarga = new DataWarga;
                $dataWarga->nama_lengkap = $request->nama_lengkap;
                $dataWarga->nama_panggilan = $request->nama_panggilan;
                $dataWarga->nik = $request->nik;
                $dataWarga->jenis_kelamin = $request->jenis_kelamin;
                $dataWarga->tempat_lahir = $request->tempat_lahir;
                $dataWarga->tanggal_lahir = $request->tanggal_lahir;
                $dataWarga->agama = $request->agama;
                $dataWarga->no_kk = $request->no_kk;
                $dataWarga->no_akta = $request->no_akta_kelahiran;
                $dataWarga->no_akta_pernikahan = $request->no_akta_pernikahan;
                $dataWarga->desa = $request->desa;
                $dataWarga->kelurahan = $request->kelurahan;

                $dataWarga->pendidikan = $request->pendidikan;
                $dataWarga->jenis_pekerjaan = $request->jenis_pekerjaan;
                $dataWarga->golongan_darah = $request->golongan_darah;
                $dataWarga->status_perkawinan = $request->status_perkawinan;
                $dataWarga->tanggal_perkawinan = $request->tanggal_perkawinan;
                $dataWarga->tanggal_perceraian = $request->tanggal_perceraian;
                $dataWarga->status_didalam_keluarga = $request->status_didalam_keluarga;
                $dataWarga->kewarganegaraan = $request->kewarganegaraan;
                $dataWarga->nik_ayah = $request->nik_ayah;
                $dataWarga->nama_lengkap_ayah = $request->nama_lengkap_ayah;
                $dataWarga->nik_ibu = $request->nik_ibu;
                $dataWarga->nama_lengkap_ibu = $request->nama_lengkap_ibu;
                $dataWarga->banjar = $request->banjar;
                $dataWarga->tempekan = $request->tempekan;
                $dataWarga->alamat_asal = $request->alamat_asal;
                $dataWarga->alamat_domisili = $request->alamat_domisili;
                $dataWarga->dusun = $request->dusun;
                $dataWarga->status_ektp = $request->status_ektp;
                $dataWarga->save();
                
              
            }else{
                DataWarga::updateOrCreate(['id' => $request->id],
                    [
                        'nama_lengkap' => $request->nama_lengkap,
                        'nama_panggilan' => $request->nama_panggilan,
                        'nik' => $request->nik,
                        'jenis_kelamin' => $request->jenis_kelamin,
                        'tempat_lahir' => $request->tempat_lahir,
                        'tanggal_lahir' => $request->tanggal_lahir,
                        'agama' => $request->agama,
                        'no_kk' => $request->no_kk,
                        'pendidikan' => $request->pendidikan,
                        'jenis_pekerjaan' => $request->jenis_pekerjaan,
                        'golongan_darah' => $request->golongan_darah,
                        'status_perkawinan' => $request->status_perkawinan,
                        'tanggal_perkawinan' => $request->tanggal_perkawinan,
                        'tanggal_perceraian' => $request->tanggal_perceraian,
                        'status_didalam_keluarga' => $request->status_didalam_keluarga,
                        'kewarganegaraan' => $request->kewarganegaraan,
                        'nik_ayah' => $request->nik_ayah,
                        'nama_lengkap_ayah' => $request->nama_lengkap_ayah,
                        'nik_ibu' => $request->nik_ibu,
                        'nama_lengkap_ibu' => $request->nama_lengkap_ibu,
                        'banjar' => $request->banjar,
                        'tempekan' => $request->tempekan,
                        'alamat_asal' => $request->alamat_asal,
                        'alamat_domisili' => $request->alamat_domisili,
                        'dusun' => $request->dusun,
                        'status_ektp' => $request->status_ektp,
                        'no_akta' => $request->no_akta_kelahiran,
                        'no_akta_pernikahan' => $request->no_akta_pernikahan,
                        'desa' => $request->desa,
                        'kelurahan' => $request->kelurahan,

                    ]
                );   
            }
            DB::commit();
            if($request->id == ''){
                return response()->json(
                    [   
                        'success' => true,
                        'title' => 'Sukses',
                        'message'=> 'Sukses menambahkan data warga'
                    ]
                );
            }else{
                return response()->json(
                    [   
                        'success' => true,
                        'title' => 'Sukses',
                        'message'=> 'Sukses update data warga'
                    ]
                );
            }
          

      
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(
                [   
                    'success' => false,
                    'title' => 'Gagal',
                    'message'=> $th->getMessage(),
                ]
            );
        }
    }

    public function detail($id){
        $data = DataWarga::find($id);

        if($data->status_didalam_keluarga == 'anak'){
            $data['ayah'] = DataWarga::where('no_kk','=',$data->no_kk)->where('status_didalam_keluarga','kepala_keluarga')->first();
            $data['ibu'] = DataWarga::where('no_kk','=',$data->no_kk)->where('status_didalam_keluarga','istri')->first();
        }
        return response()->json(
            [   
                'success' => true,
                'data' => $data
            ]
        );
    }

    public function delete($id){
        $data = DataWarga::find($id)->delete();

        return response()->json(
            [   
                'success' => true,
                'message'=> 'Sukses delete data warga',
                'data' => $data
            ]
        );
    }
}
