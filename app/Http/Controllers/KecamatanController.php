<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravolt\Indonesia\Models\Kabupaten;
use Laravolt\Indonesia\Models\Kecamatan;
use DataTables;

class KecamatanController extends Controller
{
    //
    public function index(){
        $kabupaten = Kabupaten::latest()->get();
        return view('kecamatan.index',compact('kabupaten'));
    }

    public function data(){
        $data = Kecamatan::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="editKecamatan('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" onclick="deleteKecamatan('.$row->id.')" class="btn btn-danger btn-sm deletePost"><i class="fa fa-trash-o"></i></a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);

    }

    public function store(Request $request){
        try {
            //code...
            $validated = $request->validate([
                'code' => 'required|unique:indonesia_districts,code,'. $request->id .'',
                'name' => ['required'],
            ]);
            Kecamatan::updateOrCreate(['id' => $request->id],
            [
                'city_code' => $request->city_code,
                'code' => $request->code, 
                'name' => $request->name, 
            ]);        
    
            return response()->json([
                'success'=> true,
                'message' => 'Kecamatan saved successfully.',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success'=> false,
                'message' => $th->getMessage(),
            ]);
        }
      
    }

    public function edit($id){
        $kabupaten = Kecamatan::find($id);
        return response()->json($kabupaten);
    }

    public function delete($id){
        Kecamatan::where('id', $id)->delete();

        //return response
        return response()->json([
            'success' => true,
            'message' => 'Data Kecamatan Berhasil Dihapus!.',
        ]); 
    }
}
