<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;
use DataTables;
use Illuminate\Validation\Rule;

class PermissionController extends Controller
{
    //
    public function index(){
        return view('permissions.index');
    }

    public function data(){
        $data = Permission::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" onclick="editPermissions('.$row->id.')" class="edit btn btn-primary btn-sm editPost"><i class="fa fa-edit"></i></a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" onclick="deletePermissions('.$row->id.')" class="btn btn-danger btn-sm deletePost"><i class="fa fa-trash-o"></i></a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function store(Request $request,Permission $permission){
        try {
            //code...
            $validated = $request->validate([
                'name' => 'required|unique:permissions,name,'. $request->id .'',
                'display_name' => ['required'],
                'description' => ['required'],
            ]);
            Permission::updateOrCreate(['id' => $request->id],
            [
                'name' => $request->name, 
                'display_name' => $request->display_name, 
                'description' => $request->description
            ]);        
    
            return response()->json([
                'success'=> true,
                'message' => 'Permissions saved successfully.',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success'=> false,
                'message' => $th->getMessage(),
            ]);
        }
      
    }
    public function edit($id)
    {
        $permissions = Permission::find($id);
        return response()->json($permissions);
    }

    public function delete($id){
        Permission::where('id', $id)->delete();

        //return response
        return response()->json([
            'success' => true,
            'message' => 'Data Permissions Berhasil Dihapus!.',
        ]); 
    }
}
