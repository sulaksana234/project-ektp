<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
        $repositories = [
            ['Users', 'EloquentUsers'],
            ['Role', 'EloquentRole'],
        ];
        foreach($repositories as $repository)
        {
            $this->app->bind(
                'App\Repository\Interfaces\\' . $repository[0] . 'Repository',
                'App\Repository\\' . $repository[1] . 'Repository'
            );
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
