<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataWarga extends Model
{
    public $table = 'data_warga';
    protected $fillable = [
        'id',
        'nama_lengkap',
        'nama_panggilan',
        'nik',
        'no_kk',
        'no_akta',
        'no_akta_pernikahan',
        'desa',
        'kelurahan',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'agama',
        'pendidikan',
        'jenis_pekerjaan',
        'golongan_darah',
        'status_perkawinan',
        'tanggal_perkawinan',
        'tanggal_perceraian',
        'status_didalam_keluarga',
        'kewarganegaraan',
        'nik_ayah',
        'nama_lengkap_ayah',
        'nik_ibu',
        'nama_lengkap_ibu',
        'banjar',
        'tempekan',
        'alamat_asal',
        'alamat_domisili',
        'dusun',
        'status_ektp',


    ];

}
