<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    public $table = 'agama';
    protected $fillable = [
        'id',
        'name',
        'display_name',
    ];
}
