<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GolonganDarah extends Model
{
    public $table = 'golongan_darah';
    protected $fillable = [
        'id',
        'name',
        'display_name',
    ];}
