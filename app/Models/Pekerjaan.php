<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{    public $table = 'jenis_pekerjaan';
    protected $fillable = [
        'id',
        'name',
        'display_name',
    ];
}
