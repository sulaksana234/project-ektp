<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    
    public $table = 'pendidikan';
    protected $fillable = [
        'id',
        'name',
        'display_name',
    ];
}
