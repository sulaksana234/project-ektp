<?php

use App\Http\Controllers\AgamaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthLoginRegisterController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\DataWargaController;
use App\Http\Controllers\GolonganDarahController;
use App\Http\Controllers\JenisPekerjaanController;
use App\Http\Controllers\PendidikanController;
use App\Http\Controllers\ProvinsiController;
use App\Http\Controllers\KabupatenController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\DesaController;
use App\Http\Controllers\DashboardController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::controller(AuthLoginRegisterController::class)->group(function() {
    Route::get('/register', 'register')->name('register');
    Route::post('/store', 'store')->name('store');
    Route::get('/login', 'login')->name('login');
    Route::post('/authenticate', 'authenticate')->name('authenticate');
    Route::post('/logout', 'logout')->name('logout');
});

Route::group(['middleware' => ['auth','verified']], function () { 
    Route::controller(PermissionController::class)->group(function(){
        Route::get('permissions', 'index')->name('index.permissions');
        Route::get('permissions/data', 'data')->name('data.permissions');
        Route::post('permissions/store', 'store')->name('store.permissions');
        Route::get('permissions/edit/{value}', 'edit')->name('edit.permissions');
        Route::delete('permissions/delete/{value}', 'delete')->name('delete.permissions');
    });
    Route::controller(RoleController::class)->group(function(){
        Route::get('roles', 'index')->name('index.roles');
        Route::get('roles/data', 'data')->name('data.roles');
        Route::post('roles/store', 'store')->name('store.roles');
        Route::get('roles/edit/{value}', 'edit')->name('edit.roles');
        Route::delete('roles/delete/{value}', 'delete')->name('delete.roles');
    });

    Route::controller(UsersController::class)->group(function(){
        Route::get('users', 'index')->name('index.users');
        Route::get('users/data', 'data')->name('data.users');
        Route::post('users/store', 'store')->name('store.users');
        Route::get('users/edit/{value}', 'edit')->name('edit.users');
        Route::delete('users/delete/{value}', 'delete')->name('delete.users');
    });

    Route::controller(DataWargaController::class)->group(function(){
        Route::get('data-warga', 'index')->name('index.dataWarga');
        Route::get('data-warga/data', 'data')->name('data.dataWarga');
        Route::post('data-warga/store', 'store')->name('store.dataWarga');
        Route::get('data-warga/detail/{value}', 'detail')->name('detail.dataWarga');
        Route::delete('data-warga/delete/{value}', 'delete')->name('delete.dataWarga');
        Route::post('data-warga/importDataWarga','importDataWarga')->name('importDataWarga');

    });

    Route::controller(AgamaController::class)->group(function(){
        Route::get('dataAll/agama', 'dataAll')->name('dataAll.agama');
    });

    Route::controller(PendidikanController::class)->group(function(){
        Route::get('dataAll/pendidikan', 'dataAll')->name('dataAll.pendidikan');
    });
  
    Route::controller(JenisPekerjaanController::class)->group(function(){
        Route::get('dataAll/jenisPekerjaan', 'dataAll')->name('dataAll.jenisPekerjaan');
    });

    Route::controller(GolonganDarahController::class)->group(function(){
        Route::get('dataAll/golonganDarah', 'dataAll')->name('dataAll.golonganDarah');
    });

    Route::controller(ProvinsiController::class)->group(function(){
        Route::get('province', 'index')->name('index.province');
        Route::get('province/data', 'data')->name('data.province');
        Route::post('province/store', 'store')->name('store.province');
        Route::get('province/edit/{value}', 'edit')->name('edit.province');
        Route::delete('province/delete/{value}', 'delete')->name('delete.province');
    });

    Route::controller(KabupatenController::class)->group(function(){
        Route::get('kabupaten', 'index')->name('index.kabupaten');
        Route::get('kabupaten/data', 'data')->name('data.kabupaten');
        Route::post('kabupaten/store', 'store')->name('store.kabupaten');
        Route::get('kabupaten/edit/{value}', 'edit')->name('edit.kabupaten');
        Route::delete('kabupaten/delete/{value}', 'delete')->name('delete.kabupaten');
    });

    Route::controller(KecamatanController::class)->group(function(){
        Route::get('kecamatan', 'index')->name('index.kecamatan');
        Route::get('kecamatan/data', 'data')->name('data.kecamatan');
        Route::post('kecamatan/store', 'store')->name('store.kecamatan');
        Route::get('kecamatan/edit/{value}', 'edit')->name('edit.kecamatan');
        Route::delete('kecamatan/delete/{value}', 'delete')->name('delete.kecamatan');
    });

    Route::controller(DesaController::class)->group(function(){
        Route::get('kelurahan', 'index')->name('index.kelurahan');
        Route::get('kelurahan/data', 'data')->name('data.kelurahan');
        Route::post('kelurahan/store', 'store')->name('store.kelurahan');
        Route::get('kelurahan/edit/{value}', 'edit')->name('edit.kelurahan');
        Route::delete('kelurahan/delete/{value}', 'delete')->name('delete.kelurahan');
    });

    Route::controller(JenisPekerjaanController::class)->group(function(){
        Route::get('jenisPekerjaan', 'index')->name('index.jenisPekerjaan');
        Route::get('jenisPekerjaan/data', 'data')->name('data.jenisPekerjaan');
        Route::post('jenisPekerjaan/store', 'store')->name('store.jenisPekerjaan');
        Route::get('jenisPekerjaan/edit/{value}', 'edit')->name('edit.jenisPekerjaan');
        Route::delete('jenisPekerjaan/delete/{value}', 'delete')->name('delete.jenisPekerjaan');
    });

    Route::controller(DashboardController::class)->group(function(){
        Route::get('dashboard', 'index')->name('index.dashboard');
        Route::get('dashboard/dataektp', 'dataektp')->name('dataektp.dashboard');

    });
});

