<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigatio-lavel">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                    <span class="pcoded-mtext">Dashboard</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="active">
                        <a href="/dashboard">
                            <span class="pcoded-mtext">Dashboard</span>
                        </a>
                    </li>
                    {{-- <li class="">
                        <a href="dashboard-crm.htm">
                            <span class="pcoded-mtext">CRM</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="dashboard-analytics.htm">
                            <span class="pcoded-mtext">Analytics</span>
                            <span class="pcoded-badge label label-info ">NEW</span>
                        </a>
                    </li> --}}
                </ul>
            </li>
            <li class="pcoded-hasmenu {{ Request::is('/roles*') ? 'active' : '' }} pcoded-trigger" >
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                    <span class="pcoded-mtext">Users</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="active">
                        <a href="/users">
                            <span class="pcoded-mtext">User</span>
                        </a>
                    </li>
                    @if(Auth::user()->hasRole('superadmin'))
                    <li class="">
                        <a href="/roles">
                            <span class="pcoded-mtext">Roles</span>
                        </a>
                    </li>
                    @endif
                    @if(Auth::user()->hasRole('superadmin'))
                    <li class="">
                        <a href="/permissions">
                            <span class="pcoded-mtext">Permission</span>
                            <span class="pcoded-badge label label-info ">NEW</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>

            <li class="pcoded-hasmenu pcoded-trigger" >
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                    <span class="pcoded-mtext">Database</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="{{ Request::is('/province*') ? 'active' : '' }}">
                        <a href="/province">
                            <span class="pcoded-mtext">Provinsi</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('/kabupaten*') ? 'active' : '' }}">
                        <a href="/kabupaten">
                            <span class="pcoded-mtext">Kabupaten</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('/kecamatan*') ? 'active' : '' }}">
                        <a href="/kecamatan">
                            <span class="pcoded-mtext">Kecamatan</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('/kelurahan*') ? 'active' : '' }}">
                        <a href="/kelurahan">
                            <span class="pcoded-mtext">Desa</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('/jenisPekerjaan*') ? 'active' : '' }}">
                        <a href="/jenisPekerjaan">
                            <span class="pcoded-mtext">Jenis Pekerjaan</span>
                        </a>
                    </li>
                </ul>
            </li>
         
            <li class="" {{ Request::is('/data-warga*') ? 'active' : '' }}>
                <a href="/data-warga">
                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                    <span class="pcoded-mtext">Data Warga</span>
                </a>
            </li>
          
        </ul>
  
    </div>
</nav>