<div class="container-fluid iq-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="flex-wrap d-flex justify-content-between align-items-center">
                                <div>
                                    <h1>Hello {{Auth::user()->name}}</h1>
                                    <p>@lang('dashboard.navtitle')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>