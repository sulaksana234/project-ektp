<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sistem Informasi Bayu - @yield('title')</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon icon -->
    <link rel="icon" href="/template/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="/template/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="/template/assets/icon/feather/css/feather.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="/template/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/template/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome/all.min.css">

    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="/template/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/template/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/template/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">


    <link rel="stylesheet" type="text/css" href="/template/assets/icon/font-awesome/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="/template/bower_components/pnotify/css/pnotify.css">
    <link rel="stylesheet" type="text/css" href="/template/bower_components/pnotify/css/pnotify.brighttheme.css">
    <link rel="stylesheet" type="text/css" href="/template/bower_components/pnotify/css/pnotify.buttons.css">
    <link rel="stylesheet" type="text/css" href="/template/bower_components/pnotify/css/pnotify.history.css">
    <link rel="stylesheet" type="text/css" href="/template/bower_components/pnotify/css/pnotify.mobile.css">
    <link rel="stylesheet" type="text/css" href="/template/assets/pages/pnotify/notify.css">

        <!-- Required Fremwork -->
        <link rel="stylesheet" type="text/css" href="/template/bower_components/intro.js/css/introjs.css">

    <!-- Select 2 css -->
    <link rel="stylesheet" href="/template/bower_components/select2/css/select2.min.css">
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css" href="/template/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css" href="/template/bower_components/multiselect/css/multi-select.css">
    
    <!-- Style.css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css"/>
        <!-- animation nifty modal window effects css -->
        <link rel="stylesheet" type="text/css" href="/template/assets/css/component.css">

    <style>
        .btn {
  margin-left: 10px;
  margin-right: 10px;
}
/* Boostrap Buttons Styling */

.btn-default {
  font-family: Raleway-SemiBold;
  font-size: 13px;
  color: rgba(108, 88, 179, 0.75);
  letter-spacing: 1px;
  line-height: 15px;
  border: 2px solid rgba(108, 89, 179, 0.75);
  border-radius: 40px;
  background: transparent;
  transition: all 0.3s ease 0s;
}

.btn-default:hover {
  color: #FFF;
  background: rgba(108, 88, 179, 0.75);
  border: 2px solid rgba(108, 89, 179, 0.75);
}

.btn-primary {
  font-family: Raleway-SemiBold;
  font-size: 13px;
  color: rgba(58, 133, 191, 0.75);
  letter-spacing: 1px;
  line-height: 15px;
  border: 2px solid rgba(58, 133, 191, 0.75);
  border-radius: 40px;
  background: transparent;
  transition: all 0.3s ease 0s;
}

.btn-primary:hover {
  color: #FFF;
  background: rgba(58, 133, 191, 0.75);
  border: 2px solid rgba(58, 133, 191, 0.75);
}

.btn-success {
  font-family: Raleway-SemiBold;
  font-size: 13px;
  color: rgba(103, 192, 103, 0.75);
  letter-spacing: 1px;
  line-height: 15px;
  border: 2px solid rgba(103, 192, 103, 0.75);
  border-radius: 40px;
  background: transparent;
  transition: all 0.3s ease 0s;
}

.btn-success:hover {
  color: #FFF;
  background: rgb(103, 192, 103, 0.75);
  border: 2px solid rgb(103, 192, 103, 0.75);
}

.btn-info {
  font-family: Raleway-SemiBold;
  font-size: 13px;
  color: rgba(91, 192, 222, 0.75);
  letter-spacing: 1px;
  line-height: 15px;
  border: 2px solid rgba(91, 192, 222, 0.75);
  border-radius: 40px;
  background: transparent;
  transition: all 0.3s ease 0s;
}

.btn-info:hover {
  color: #FFF;
  background: rgba(91, 192, 222, 0.75);
  border: 2px solid rgba(91, 192, 222, 0.75);
}

.btn-warning {
  font-family: Raleway-SemiBold;
  font-size: 13px;
  color: rgba(240, 173, 78, 0.75);
  letter-spacing: 1px;
  line-height: 15px;
  border: 2px solid rgba(240, 173, 78, 0.75);
  border-radius: 40px;
  background: transparent;
  transition: all 0.3s ease 0s;
}

.btn-warning:hover {
  color: #FFF;
  background: rgb(240, 173, 78, 0.75);
  border: 2px solid rgba(240, 173, 78, 0.75);
}

.btn-danger {
  font-family: Raleway-SemiBold;
  font-size: 13px;
  color: rgba(217, 83, 78, 0.75);
  letter-spacing: 1px;
  line-height: 15px;
  border: 2px solid rgba(217, 83, 78, 0.75);
  border-radius: 40px;
  background: transparent;
  transition: all 0.3s ease 0s;
}

.btn-danger:hover {
  color: #FFF;
  background: rgba(217, 83, 78, 0.75);
  border: 2px solid rgba(217, 83, 78, 0.75);
}

.form-control:focus {
    box-shadow: none;
    border-color: #BA68C8
}

.profile-button {
    background: rgb(99, 39, 120);
    box-shadow: none;
    border: none
}

.profile-button:hover {
    background: #682773
}

.profile-button:focus {
    background: #682773;
    box-shadow: none
}

.profile-button:active {
    background: #682773;
    box-shadow: none
}

.back:hover {
    color: #682773;
    cursor: pointer
}

.labels {
    font-size: 11px
}

.add-experience:hover {
    background: #BA68C8;
    color: #fff;
    cursor: pointer;
    border: solid 1px #BA68C8
}

.mt-100{margin-top: 100px}body{background: #00B4DB;background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);background: linear-gradient(to right, #0083B0, #00B4DB);color: #514B64;min-height: 100vh}
    </style>
</head>

<body>
    <!-- Pre-loader start -->
    @include('layouts.backend.loading')
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a href="index-1.htm">
                            <img class="img-fluid" src="/assets/images/logo.png" style="height: 100px;" alt="Theme-Logo">
                        </a>
                        <a class="mobile-options">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="feather icon-maximize full-screen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-bell"></i>
                                        <span class="badge bg-c-pink">5</span>
                                    </div>
                                    <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="/template/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">John Doe</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="/template/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Joseph William</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="/template/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Sara Soudein</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-message-square"></i>
                                        <span class="badge bg-c-green">3</span>
                                    </div>
                                </div>
                            </li>
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="/template/assets/images/avatar-4.jpg" class="img-radius" alt="User-Profile-Image">
                                        <span>{{ Auth::user()->name }}</span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <a href="#!">
                                                <i class="feather icon-settings"></i> Settings
                                            </a>
                                        </li>
                                        <li>
                                            <a href="user-profile.htm">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="email-inbox.htm">
                                                <i class="feather icon-mail"></i> My Messages
                                            </a>
                                        </li>
                                        <li>
                                            <a href="auth-lock-screen.htm">
                                                <i class="feather icon-lock"></i> Lock Screen
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                       
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();"
                                                >Logout</a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                                    @csrf
                                                </form>
                                            
                                        </li>
                                      
                                    </ul>

                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <!-- Sidebar chat start -->
            @include('layouts.backend.sidebar')
            <!-- Sidebar inner chat start-->
            <div class="showChat_inner">
                <div class="media chat-inner-header">
                    <a class="back_chatBox">
                        <i class="feather icon-chevron-left"></i> Josephin Doe
                    </a>
                </div>
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="#!">
                        <img class="media-object img-radius img-radius m-t-5" src="/template/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                    </a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                </div>
                <div class="media chat-messages">
                    <div class="media-body chat-menu-reply">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                    <div class="media-right photo-table">
                        <a href="#!">
                            <img class="media-object img-radius img-radius m-t-5" src="/template/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                        </a>
                    </div>
                </div>
                <div class="chat-reply-box p-b-20">
                    <div class="right-icon-control">
                        <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                        <div class="form-icon">
                            <i class="feather icon-navigation"></i>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    @include('layouts.backend.navsidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">

                                  @yield('content')
                                </div>

                                <div id="styleSelector">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="../files/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="../files/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="../files/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="../files/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="../files/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
  <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="/template/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="/template/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/template/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="/template/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="/template/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="/template/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="/template/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="/template/bower_components/chart.js/js/Chart.js"></script>
    <!-- amchart js -->
    <script src="/template/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="/template/assets/pages/widget/amchart/serial.js"></script>
    <script src="/template/assets/pages/widget/amchart/light.js"></script>
    <script src="/template/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="/template/assets/js/SmoothScroll.js"></script>
    <script src="/template/assets/js/pcoded.min.js"></script>
    <!-- data-table js -->
    <script src="/template/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/template/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/template/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="/template/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="/template/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="/template/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/template/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/template/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/template/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/template/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="/template/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="/template/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="/template/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="/template/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->

    <script src="/template/assets/js/pcoded.min.js"></script>
    <script src="/template/assets/js/vartical-layout.min.js"></script>
    <script src="/template/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="/template/assets/js/script.js"></script>
    <script type="text/javascript" src="/template/assets/pages/dashboard/custom-dashboard.js"></script>
    <script type="text/javascript" src="/template/assets/js/script.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->

    <!-- pnotify js -->
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.js"></script>
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.desktop.js"></script>
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.buttons.js"></script>
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.confirm.js"></script>
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.callbacks.js"></script>
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.animate.js"></script>
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.history.js"></script>
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.mobile.js"></script>
    <script type="text/javascript" src="/template/bower_components/pnotify/js/pnotify.nonblock.js"></script>
    <script type="text/javascript" src="/template/assets/pages/pnotify/notify.js"></script>

    <!-- jquery slimscroll js -->
<script type="text/javascript" src="/template/bower_components/intro.js/js/intro.js"></script>
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>

<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<!-- Select 2 js -->
<script type="text/javascript" src="/template/bower_components/select2/js/select2.full.min.js"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="/template/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js">


</script>

<script type="text/javascript" src="/template/bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="/template/assets/js/jquery.quicksearch.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="/template/assets/pages/advance-elements/select2-custom.js"></script>

    <!-- sweet alert js -->
    <script type="text/javascript" src="/template/bower_components/sweetalert/js/sweetalert.min.js"></script>
    <script type="text/javascript" src="/template/assets/js/modal.js"></script>
        <!-- modalEffects js nifty modal window effects -->
        <script type="text/javascript" src="/template/assets/js/modalEffects.js"></script>
        <script type="text/javascript" src="/template/assets/js/classie.js"></script>
<script>
        introJs().start();


  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
      $('[data-toggle="popover"]').popover()
      $('.collapse').collapse()
</script>

@yield('scripts');

</body>

</html>



