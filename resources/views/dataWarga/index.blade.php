@extends('layouts.backend.index')

@section('content')
<div class="page-body">
    <div class="row">
        <div class="col-md-12">
          
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Ajax data source (Arrays) table start -->
                        <div class="card">
                            <div
                            class="card-header with-border d-flex justify-content-start align-items-center media_card_header">
                            @if(Auth::user()->hasRole('superadmin'))

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-5">
                                        <h5>Data Warga</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" type="button" class="btn btn-primary btn-sm" onclick="tambahWarga()">Tambah Warga</a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="" class="btn btn-primary">Export
                                            Demo
                                            file</a>
                                    </div>
                                    <div class="col-md-3">
                                        <form action="{{ route('importDataWarga') }}" method="POST"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="input-group">
                                                <input type="file" name="file" class="form-control" required
                                                    aria-describedby="basic-addon2">
                                                <button type="submit" class="input-group-text btn btn-primary"
                                                    id="basic-addon2">Import</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h5>Data Warga</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" type="button" class="btn btn-primary btn-sm" onclick="tambahWarga()">Tambah Warga</a>
                                    </div>
                               
                                </div>
                            </div>
                            @endif


                        </div>
                            <div class="card-block">
                                <div class="table-responsive dt-responsive">
                                    <table id="warga" class="table table-striped table-bordered nowrap">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NO KK</th>
                                                <th>NO KTP</th>
                                                <th>NAMA</th>
                                                <th>NAMA PANGGILAN</th>
                                                <th>NIK</th>
                                                <th>JENIS KELAMIN</th>

                                                <th>Action.</th>
                                             
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detailWarga" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Data Warga</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
            </div>
            <div class="modal-body">
                <div class="container rounded bg-white">
                    <div class="row">
                   
                        <div class="col-md-12 border-right">
                            <div>
                                <div class="d-flex flex-column align-items-center text-center"><img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"><span class="font-weight-bold" id="textNamaLengkap">Edogaru</span><span class="text-black-50" id="textNamaPanggilan">edogaru@mail.com.my</span><span> </span></div>

                                <div class="d-flex justify-content-between align-items-center mb-3">
                                    <h4 class="text-right">Profile Settings</h4>
                                </div>
                                <form id="wargaForm" name="wargaForm" class="form-horizontal">

                                <div class="row mt-2">
                                    <input type="hidden" name="id" id="id">
                                    <div class="col-md-12"><label class="labels">NO KK</label><input type="text" class="form-control" id="no_kk" name="no_kk" placeholder="No KK" value=""></div>
                                    <div class="col-md-12"><label class="labels">NAMA LENGKAP</label><input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap" value=""></div>

                                    <div class="col-md-12"><label class="labels">NAMA PANGGILAN</label><input type="text" class="form-control" value="" id="nama_panggilan" name="nama_panggilan" placeholder="Nama Panggilan"></div>
                                    <div class="col-md-12"><label class="labels">NIK</label><input type="text" class="form-control" placeholder="NIK..." value="" id="no_akta_kelahiranedit" name="no_akta_kelahiran"></div>
                                    <div class="col-md-12"><label class="labels">NO AKTA KELAHIRAN</label><input type="text" class="form-control" placeholder="NIK..." value="" id="nik" name="nik"></div>

                                    <div class="col-md-12"><label class="labels">JENIS KELAMIN</label><br>
    

                                        <label for=""><input id="checkPerempuan" type="radio"class="form-control" name="jenis_kelamin" value="perempuan" {{ (old('jenis_kelamin') == 'perempuan') ? 'checked' : '' }} >Perempuan</label>
                                        <label for=""><input id="checkLaki" type="radio"class="form-control" name="jenis_kelamin" value="laki_laki" {{ (old('jenis_kelamin') == 'laki_laki') ? 'checked' : '' }} >Laki Laki</label>
                                    </div>
                                    <div class="col-md-12"><label class="labels">TEMPAT LAHIR</label><input type="text" class="form-control" placeholder="Tempat Lahir" id="tempat_lahir" name="tempat_lahir" value=""></div>
                                    <div class="col-md-12"><label class="labels">TANGGAL LAHIR</label><input type="date" class="form-control" placeholder="Tanggal Lahir" id="tanggal_lahirdetail" name="tanggal_lahir" value=""></div>
                                    <div class="col-md-12"><label class="labels">AGAMA</label>
                                        <select name="agama" id="dataAgama" class="form-control form-select">
                                            <option value="" disabled>== Pilih Agama ==</option>
                                        
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="labels">PENDIDIKAN</label>
                                     
                                        <select name="pendidikan" id="dataPendidikan" class="form-control form-select">
                                            <option value="" disabled>== Pilih Pendidikan ==</option>
                                                                                 
                                        </select>
                                          
                                    </div>
                                    <div class="col-md-12">
                                        <label class="labels">Jenis Pekerjaan</label>
                                        <select name="jenis_pekerjaan" id="dataPekerjaan" class="form-control form-select">
                                            <option value="" disabled>== Pilih Pekerjaan ==</option>                      
                                        </select>                                    
                                    </div>
                                    <div class="col-md-12">
                                        <label class="labels">Golongan Darah</label>
                                        <select name="golongan_darah" id="dataGolonganDarah" class="form-control form-select">
                                            <option value="" disabled>== Pilih Golongan Darah ==</option>                      
                                        </select>                                    
                                    </div>
                                    <div class="col-md-12">
                                        <label for="status_perkawinan" class="labels">Status Perkawinan</label>
                                            <select name="status_perkawinan" id="statusPerkawinanDetail" class="form-control form-select" onchange="pilihStatusPerkawinanDetail()">
                                                <option value="" disabled>== Pilih Status Perkawinan ==</option>
                                                <option value="belum_kawin">Belum Kawin</option>
                                                <option value="kawin_tercatat">Kawin Tercatat</option>
                                                <option value="kawin_belum_tercatat">Kawin Belum Tercatat</option>
                                                <option value="cerai_hidup">Cerai Hidup</option>
                                                <option value="cerai_mati">Cerai Mati</option>
                                            </select>
                                    </div>      
                                   
                                    <div class="col-md-8" id="tanggalPerkawinanEdit" style="display: none;">
                                        <label for="tanggal_perkawinan" class="labels">TANGGAL PERKAWINAN</label>
                                            <input type="date" name="tanggal_perkawinan" id="tanggal_perkawinanedit" class="form-control">
                                    </div>
                                    <div class="col-md-4" id="no_akta_perkawinanEdit" style="display: none;">
                                        <label for="no_akta_perkawinan" class="labels">NO AKTA PERKAWINAN</label>
                                            <input type="text" name="no_akta_pernikahan" id="no_akta_pernikahanedit" class="form-control">
                                    </div>
                                    <div class="col-md-12" id="tanggalPerceraianEdit" style="display: none;">
                                        <label for="tanggal_perceraian" class="labels">TANGGAL PERCERAIAN</label>
                                            <input type="date" name="tanggal_perceraian" id="tanggal_perceraianedit" class="form-control">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="statusDidalamKeluarga" class="labels">STATUS DIDALAM KELUARGA</label>
                                            <select name="status_didalam_keluarga" id="statusDidalamKeluarga" class="form-control form-select" onchange="pilihStatusPerkawinan()">
                                                <option value="" disabled>== Pilih Status Keluarga ==</option>
                                                <option value="kepala_keluarga">KEPALA KELUARGA</option>
                                                <option value="bapak">BAPAK</option>
                                                <option value="ibu">IBU</option>
                                                <option value="anak">ANAK</option>
                                                <option value="kakek">KAKEK</option>
                                                <option value="nenek">NENEK</option>
                                                <option value="cucu">CUCU</option>
                                                <option value="family_lain">FAMILI LAIN</option>
                                                <option value="lainnya">LAINNYA</option>
                                            </select>
                                    </div>   
                                    <div class="col-md-12">
                                        <label for="kewarganegaraan" class="labels">KEWARGANEGARAAN</label>
                                            <select name="kewarganegaraan" id="kewarganegaraan" class="form-control form-select" onchange="pilihKewarganegaraan()">
                                                <option value="" disabled>== Pilih Kewarganegaraan ==</option>
                                                <option value="wni"> WNI </option>
                                                <option value="wna"> WNA </option>
                
                                            </select>
                                    </div>     
                                    <div class="col-md-12">
                                        <label for="nik_ayah" class="labels">NIK AYAH</label><br>
                                        <input type="text" name="nik_ayah" id="nik_ayah" class="form-control">
                                    </div>   
                                    <div class="col-md-12">
                                        <label for="nama_lengkap_ayah" class="labels">NAMA LENGKAP AYAH</label><br>
                                        <input type="text" name="nama_lengkap_ayah" id="nama_lengkap_ayah" class="form-control">
                                    </div>   
                                    <div class="col-md-12">
                                        <label for="nik_ibu" class="labels">NIK IBU</label><br>
                                        <input type="text" name="nik_ibu" id="nik_ibu" class="form-control">
                                    </div>      
                                    <div class="col-md-12">
                                        <label for="nama_lengkap_ibu" class="labels">NAMA LENGKAP IBU</label><br>
                                        <input type="text" name="nama_lengkap_ibu" id="nama_lengkap_ibu" class="form-control">
                                    </div>    
                                    <div class="col-md-12">
                                        <label for="kelurahan" class="labels">KELURAHAN</label><br>
                                        <input type="text" class="form-control @error('kelurahan') is-invalid @enderror" id="kelurahan_detail" name="kelurahan" value="{{ old('kelurahan') }}">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="alamat_domisili" class="labels">DESA</label><br>
                                        <input type="text" class="form-control @error('desa') is-invalid @enderror" id="desa_detail" name="desa" value="{{ old('desa') }}">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="tanggal_lahir" class="labels">BANJAR</label><br>
                                        <select name="banjar" id="banjarDetail" class="form-control">
                                            <option value="kertha_pascima">Kertha Pascima</option>
                                            <option value="purwa_santhi">Purwa Santhi</option>
                                            <option value="tengah">Tengah</option>
                                            <option value="anyar">Anyar</option>
                                        </select>                                    
                                    </div>
                                    <div class="col-md-12">
                                        <label for="tanggal_lahir" class="labels">TEMPEKAN</label><br>
                                      
                                         <select name="tempekan" id="tempekanDetail" class="form-control">
                                            <option value="1">Tempekan 1</option>
                                            <option value="2">Tempekan 2</option>
                                            <option value="3">Tempekan 3</option>
                                            <option value="4">Tempekan 4</option>
                                            <option value="5">Warga Administrasi</option>
                                            <option value="6">Warga Pendatang</option>
            
                                         </select>                                 
                                    </div> 
                                    <div class="col-md-12">
                                        <label for="tanggal_lahir" class="labels">ALAMAT ASAL</label><br>
                                        <textarea class="form-control" rows="5" id="alamat_asal" name="alamat_asal"></textarea>                               
                                    </div>
                                    <div class="col-md-12">
                                        <label for="alamat_domisili" class="labels">ALAMAT DOMISILI</label><br>
                                        <textarea class="form-control" rows="5" id="alamat_domisili" name="alamat_domisili"></textarea>                               
                                    </div>    
                                    
                                    <div class="col-md-12" id="status-ektpdetail">
                                        <label for="alamat_domisili" class="labels">STATUS E-KTP</label><br>
                                        <select name="status_ektp" id="status_ektpdetailvalue" class="form-control">
                                            <option value="">Pilih Status Ektp</option>
                                            <option value="no_ektp">Belum Memiliki</option>
                                            <option value="done">Sudah Memiliki</option>
                                         </select>                                    </div>                   
                                </div>
                            
                                <div class="mt-5 text-center"><button class="btn btn-primary profile-button" id="saveWarga" type="button">Save Profile</button></div>
                                </form>
                            </div>
                        </div>
                    
                    </div>
                </div>
                </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modalWarga" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Form Warga</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
            </div>
            <div class="modal-body">
                <form id="wargaFormCreate" name="wargaFormCreate" class="form-horizontal">
                    <div class="alert alert-danger" style="display:none"></div>

                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">NO KK</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="no_kk" name="no_kk" placeholder="No KK" value="" value="{{ old('no_kk') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">NIK</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK" value="" value="{{ old('nik') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">NO AKTA KELAHIRAN</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="no_akta_kelahiran" name="no_akta_kelahiran" placeholder="No Akta Kelahiran" value="" value="{{ old('no_akta') }}">
                        </div>
                    </div>
                     <div class="form-group">
                         <label for="name" class="col-sm-4 control-label">NAMA LENGKAP</label>
                         <div class="col-sm-12">
                             <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" id="nama_lengkap" name="nama_lengkap" value="{{ old('nama_lengkap') }}">

                         </div>
                     </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">NAMA PANGGILAN</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nama_panggilan" name="nama_panggilan" placeholder="Nama Panggilan" value="" value="{{ old('nama_panggilan') }}">
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">JENIS KELAMIN</label>
                        <div class="col-sm-12">
                            <label><input type="checkbox" name="jenis_kelamin" id="jenis_kelamin" value="laki_laki"> Laki Laki</label>
                            <label><input type="checkbox" name="jenis_kelamin" id="jenis_kelamin" value="perempuan"> Perempuan</label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tempat_lahir" class="col-sm-4 control-label">TEMPAT LAHIR</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir" value="" value="{{ old('tempat_lahir') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir" class="col-sm-4 control-label">TANGGAL LAHIR</label>
                        <div class="col-sm-12">
                            <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir" value="" value="{{ old('tanggal_lahir') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir" class="col-sm-4 control-label">AGAMA</label>
                        <div class="col-sm-12">
                            <select name="agama" id="agama" class="form-control form-select">
                                <option value="" disabled>== Pilih Agama ==</option>
                                @foreach($agama as $agama)
                                <option value="{{$agama->name}}">{{$agama->display_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
      
                    <div class="form-group">
                        <label for="tanggal_lahir" class="col-sm-4 control-label">PENDIDIKAN</label>
                        <div class="col-sm-12">
                            <select name="pendidikan" id="pendidikan" class="form-control form-select">
                                <option value="" disabled>== Pilih Pendidikan ==</option>
                                @foreach($pendidikan as $pendidikan)
                                <option value="{{$pendidikan->name}}">{{$pendidikan->display_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tanggal_lahir" class="col-sm-4 control-label">JENIS PEKERJAAN</label>
                        <div class="col-sm-12">
                            <select name="jenis_pekerjaan" id="dataPekerjaan" class="form-control form-select">
                                <option value="" disabled>== Pilih Jenis Pekerjaan ==</option>
                                @foreach($pekerjaan as $pekerjaan)
                                <option value="{{$pekerjaan->name}}">{{$pekerjaan->display_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tanggal_lahir" class="col-sm-4 control-label">GOLONGAN DARAH</label>
                        <div class="col-sm-12">
                            <select name="golongan_darah" id="golonganDarah" class="form-control form-select">
                                <option value="" disabled>== Pilih Jenis Pekerjaan ==</option>
                                @foreach($golonganDarah as $golonganDarah)
                                <option value="{{$golonganDarah->name}}">{{$golonganDarah->display_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tanggal_lahir" class="col-sm-4 control-label">STATUS PERKAWINAN</label>
                        <div class="col-sm-12">
                            <select name="status_perkawinan" id="statusPerkawinanTambah" class="form-control form-select" onchange="pilihStatusPerkawinan()">
                                <option value="" disabled>== Pilih Status Perkawinan ==</option>
                                <option value="belum_kawin">Belum Kawin</option>
                                <option value="kawin_tercatat">Kawin Tercatat</option>
                                <option value="kawin_belum_tercatat">Kawin Belum Tercatat</option>
                                <option value="cerai_hidup">Cerai Hidup</option>
                                <option value="cerai_mati">Cerai Mati</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="tanggalPerkawinanTambah" style="display: none;">
                        <div class="row col-md-12">
                            <label for="tanggal_lahir" class="col-sm-12 control-label">TANGGAL PERKAWINAN</label>
                            <div class="col-sm-8">
                                <input type="date" name="tanggal_perkawinan" id="tanggal_perkawinan_tambah" class="form-control">
                            </div>
                            <div class="col-sm-4" id="no_akta_pernikahan" style="display:none;">
                                <input type="text" name="no_akta_pernikahan" id="no_akta_pernikahan_tambah" class="form-control" placeholder="No Akta Pernikahan">
                            </div>
                        </div>
                   
                    </div>
                    <div class="form-group" id="tanggalPerceraianTambah" style="display: none;">
                        <label for="tanggal_perceraian" class="col-sm-4 control-label">TANGGAL PERCERAIAN</label>
                        <div class="col-sm-12">
                            <input type="date" name="tanggal_perceraian" id="tanggal_perceraian_tambah" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir" class="col-sm-4 control-label">STATUS DIDALAM KELUARGA</label>
                        <div class="col-sm-12">
                            <select name="status_didalam_keluarga" id="statusDidalamKeluarga" class="form-control form-select" onchange="pilihStatusDidalamKeluarga()">
                                <option value="" disabled>== Pilih Status Keluarga ==</option>
                                <option value="kepala_keluarga">Kepala Keluarga</option>
                                <option value="bapak">BAPAK</option>
                                <option value="ibu">IBU</option>
                                <option value="anak">ANAK</option>
                                <option value="kakek">KAKEK</option>
                                <option value="nenek">NENEK</option>
                                <option value="cucu">CUCU</option>
                                <option value="family_lain">FAMILI LAIN</option>
                                <option value="lainnya">LAINNYA</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir" class="col-sm-4 control-label">KEWARGANEGARAAN</label>
                        <div class="col-sm-12">
                            <select name="kewarganegaraan" id="kewarganegaraan" class="form-control form-select" onchange="pilihKewarganegaraan()">
                                <option value="" disabled>== Pilih Kewarganegaraan ==</option>
                                <option value="wni"> WNI </option>
                                <option value="wna"> WNA </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">NIK AYAH</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nik_ayah" name="nik_ayah" placeholder="Nik ayah..." value="" value="{{ old('nik_ayah') }}">
                        </div>
                    </div>
                     <div class="form-group">
                         <label for="name" class="col-sm-4 control-label">NAMA LENGKAP AYAH</label>
                         <div class="col-sm-12">
                             <input type="text" class="form-control @error('nama_lengkap_ayah') is-invalid @enderror" id="nama_lengkap_ayah" name="nama_lengkap_ayah" value="{{ old('nama_lengkap_ayah') }}">

                         </div>
                     </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">NIK IBU</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nik_ibu" name="nik_ibu" placeholder="Nik ibu..." value="" value="{{ old('nik_ibu') }}">
                        </div>
                    </div>
                     <div class="form-group">
                         <label for="name" class="col-sm-4 control-label">NAMA LENGKAP IBU</label>
                         <div class="col-sm-12">
                             <input type="text" class="form-control @error('nama_lengkap_ibu') is-invalid @enderror" id="nama_lengkap_ibu" name="nama_lengkap_ibu" value="{{ old('nama_lengkap_ibu') }}">
                         </div>
                     </div>
                     <div class="form-group">
                        <label for="desa" class="col-sm-4 control-label">KELURAHAN</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control @error('kelurahan') is-invalid @enderror" id="kelurahan" name="kelurahan" value="{{ old('kelurahan') }}">
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="desa" class="col-sm-4 control-label">DESA</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control @error('desa') is-invalid @enderror" id="desa" name="desa" value="{{ old('desa') }}">
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">LINGKUNGAN / DUSUN</label>
                        <div class="col-sm-12">
                            <select name="banjar" id="banjar" class="form-control">
                                <option value="kertha_pascima">Kertha Pascima</option>
                                <option value="purwa_santhi">Purwa Santhi</option>
                                <option value="tengah">Tengah</option>
                                <option value="anyar">Anyar</option>
                            </select>
                        </div>
                    </div>
                     <div class="form-group">
                         <label for="name" class="col-sm-4 control-label">Tempekan</label>
                         <div class="col-sm-12">
                             <select name="tempekan" id="tempekan" class="form-control">
                                <option value="1">Tempekan 1</option>
                                <option value="2">Tempekan 2</option>
                                <option value="3">Tempekan 3</option>
                                <option value="4">Tempekan 4</option>
                                <option value="5">Warga Administrasi</option>
                                <option value="6">Warga Pendatang</option>

                             </select>
                         </div>
                     </div>
                     <div class="form-group">
                        <label for="alamat_asal" class="col-sm-4 control-label">Alamat Asal</label>
                        <div class="col-sm-12">
                            <textarea class="form-control" rows="5" id="alamat_asal" name="alamat_asal"></textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alamat_asal" class="col-sm-4 control-label">Alamat Domisili</label>
                        <div class="col-sm-12">
                            <textarea class="form-control" rows="5" id="alamat_domisili" name="alamat_domisili"></textarea>

                        </div>
                    </div>
  
                    <div class="form-group" id="status_ektp" style="display: none;">
                        <label for="name" class="col-sm-4 control-label">Status E-KTP</label>
                        <div class="col-sm-12">
                            <select name="status_ektp" id="status_ektp" class="form-control">
                               <option value="">Pilih Status Ektp</option>
                               <option value="no_ektp">Belum Memiliki</option>
                               <option value="done">Sudah Memiliki</option>
                            </select>
                        </div>
                    </div>
       

                     <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-primary waves-effect waves-light" id="saveWarga" onclick="submitForm()"><i class="fa fa-save"></i> Save changes</button>
                     </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
<script>
    var arrayRolesSelected = [];
    $(document).ready(function(){
        fetchDataWarga();
        fetchDataAgama();
        fetchDataPendidikan();
        fetchJenisPekerjaan();
        fetchGolonganDarah();

        window.onload=function(){
            $('#tanggal_lahir').on('change', function() {
                var dob = new Date(this.value);
                var today = new Date();
                var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                console.log(age);
                if(age >= 17){
                    $("#status_ektp").show();
                }
            });

            $("#banjar").on('change', function() {
                if($("#banjar").val() == 'ling_kertha_pascima'){
                    $("#tempekan").val("");
                }
            });
        }

    });


    $(document).keydown(function(event){
        // if(event.ctrlKey && event.keyCode == nCode) {
        //     tambahUsers();
        // }
    });


    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveWarga').click(function (e) {
            e.preventDefault();
            $(this).html('<img src="/img/elipseloading.gif"></img>');
                $.ajax({
                    data: $('#wargaForm').serialize(),
                    url: "{{ route('store.dataWarga') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        if(data.success){
                            new PNotify({
                                title: data.title,
                                text: data.message,
                                icon: 'icofont icofont-info-circle',
                                type: 'success'
                            });
                            $("#modalWarga").modal("hide");
                            $('#wargaForm').trigger("reset");
                            $('#saveWarga').html('Save Changes');
                            $("#detailWarga").modal("hide");

                            fetchDataWarga();

                        }else{
                            new PNotify({
                                title: data.title,
                                text: data.message,
                                icon: 'icofont icofont-info-circle',
                                type: 'error'
                            });
                            $('#saveWarga').html('Save Changes');

                            fetchDataWarga();

                            $('.alert-danger').html('');

                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>'+data.message+'</li>');


                        }
                    
                    
                    },
                    error: function (data) {
                        $('#saveWarga').html('Save Changes');
                    }
            });
        });
    });

function pilihStatusPerkawinanDetail(){
    statusPerkawinan = $("#statusPerkawinanDetail").val();

    if(statusPerkawinan == 'kawin_tercatat'){
        $("#tanggalPerkawinanEdit").show();
        $("#tanggalPerceraianEdit").hide();
 
        $("#optionAyahEdit").show();
        $("#optionIbuEdit").show();
        $("#no_akta_perkawinanEdit").show();

    }
    else if(statusPerkawinan == 'kawin_belum_tercatat'){
        $("#tanggalPerkawinanEdit").show();
        $("#tanggalPerceraianEdit").hide();
        $("#no_akta_perkawinanEdit").hide();

        $("#optionAyahEdit").show();
        $("#optionIbuEdit").show();

    }
    else if(statusPerkawinan == 'cerai_hidup' || statusPerkawinan == 'cerai_meninggal'){
        $("#tanggalPerkawinanEdit").hide();
        $("#tanggalPerceraianEdit").show();

        $("#optionAyahEdit").show();
        $("#optionIbuEdit").show();
    } 
    else{
        $("#tanggalPerkawinanEdit").hide();
        $("#tanggalPerceraianEdit").hide();
        $("#no_akta_perkawinanEdit").hide();

        $("#optionAyahEdit").hide();
        $("#optionIbuEdit").hide();
    }
}

function pilihStatusPerkawinan(){
    var statusPerkawinan;
    statusPerkawinan = $("#statusPerkawinanTambah").val();
    console.log(statusPerkawinan);

    if(statusPerkawinan == 'kawin_tercatat'){
        $("#tanggalPerkawinanTambah").show();
        $("#no_akta_pernikahan").show();
    

    }else if(statusPerkawinan == 'kawin_belum_tercatat'){
        $("#tanggalPerkawinanTambah").show();
        $("#no_akta_pernikahan").hide();
    

    }
    else if(statusPerkawinan == 'cerai_hidup' || statusPerkawinan == 'cerai_meninggal'){
        $("#tanggalPerkawinanTambah").hide();
        $("#tanggalPerceraianTambah").show();
        $("#optionAyahTambah").show();
        $("#optionIbuTambah").show();
   
    } 
    else{
        $("#tanggalPerkawinanTambah").hide();
        $("#tanggalPerceraianTambah").hide();
        $("#optionAyahTambah").hide();
        $("#optionIbuTambah").hide();
    }
}

function fetchDataAgama(){
    $.ajax({
			type: 'GET', 
			url: '/dataAll/agama', 
			success: function(response) { 
                $.each(response, function(index, item) {
                    $("#dataAgama").append(new Option(item.display_name, item.name));
                });
			}
		});
}

function fetchJenisPekerjaan(){
    $.ajax({
			type: 'GET', 
			url: '/dataAll/jenisPekerjaan', 
			success: function(response) { 
                $.each(response, function(index, item) {
                    $("#dataPekerjaan").append(new Option(item.display_name, item.name));
                });
			}
		});
}

function fetchDataPendidikan(){
    $.ajax({
			type: 'GET', 
			url: '/dataAll/pendidikan', 
			success: function(response) { 
                $.each(response, function(index, item) {
                    $("#dataPendidikan").append(new Option(item.display_name, item.name));
                });
			}
		});
}

function fetchGolonganDarah(){
    $.ajax({
			type: 'GET', 
			url: '/dataAll/golonganDarah', 
			success: function(response) { 
                $.each(response, function(index, item) {
                    $("#dataGolonganDarah").append(new Option(item.display_name, item.name));
                });
			}
		});
}

function deleteWarga(value){
    Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Kamu akan menghapus data warga ini !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `/data-warga/delete/`+value,
                type: "DELETE",
                cache: false,
           
                success:function(response){ 

                    //show success message
                    Swal.fire({
                        type: 'success',
                        icon: 'success',
                        title: `${response.message}`,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    fetchDataWarga();

                    //remove post on table
                }
            });
        }
    })
}


function fetchDataWarga(){
    $('#warga').DataTable().destroy();

    $('#warga').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/data-warga/data",
        "columns":[
            
            {"data": 'DT_RowIndex', "name": 'DT_RowIndex'},
            {"data": 'no_kk', "name": 'no_kk'},
            {"data": 'nik', "name": 'nik'},
            {"data": 'nama_lengkap', "name": 'nama_lengkap'},
            {"data": 'nama_panggilan',render:function(data,index,row){
                if(row.nama_panggilan != null){
                    return row.nama_panggilan;
                }else{
                    return '...';
                }
            }},
            {"data": 'nik',render:function(data,index,row){
                if(row.nik != null){
                    return row.nik;
                }else{
                    return '...';
                }
            }},
            {"data": 'jenis_kelamin',render:function(data,index,row){
                if(row.jenis_kelamin != null){
                    return row.jenis_kelamin;
                }else{
                    return '...';
                }
            }},
            {"data": 'action', "name": 'action'},

        ],
    }).draw();
}

function submitForm(){
            $("#saveWarga").html('<img src="/img/elipseloading.gif"></img>');
                $.ajax({
                    data: $('#wargaFormCreate').serialize(),
                    url: "{{ route('store.dataWarga') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        if(data.success){
                            new PNotify({
                                title: data.title,
                                text: data.message,
                                icon: 'icofont icofont-info-circle',
                                type: 'success'
                            });
                            $("#modalWarga").modal("hide");
                            $('#wargaFormCreate').trigger("reset");
                            $('#saveWarga').html('Save Changes');
                            $("#detailWarga").modal("hide");

                            fetchDataWarga();

                        }else{
                            new PNotify({
                                title: data.title,
                                text: data.message,
                                icon: 'icofont icofont-info-circle',
                                type: 'error'
                            });
                            $('#saveWarga').html('Save Changes');

                            fetchDataWarga();

                            $('.alert-danger').html('');

                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>'+data.message+'</li>');


                        }
                    
                    
                    },
                    error: function (data) {
                        $('#saveWarga').html('Save Changes');
                    }
            });
}

function detailWarga(value){
    $.get("data-warga/detail/" + value , function (data) {
        $("#detailWarga").modal("show");
        $("#id").val(data.data.id);
        $('#modelHeading').html("Data Warga "+data.data.nama_lengkap);
        $("#no_kk").val(data.data.no_kk);
        $("#nama_lengkap").val(data.data.nama_lengkap);
        $("#nama_panggilan").val(data.data.nama_panggilan);
        $("#nik").val(data.data.nik);
        if(data.data.jenis_kelamin == "laki_laki"){
            $('#checkLaki').prop('checked', true);
        }else{
            $('#checkPerempuan').prop('checked', true);
        }
        $("#tempat_lahir").val(data.data.tempat_lahir);
        $("#tanggal_lahirdetail").val(data.data.tanggal_lahir);

        var dob = new Date($("#tanggal_lahirdetail").val());
                var today = new Date();
                var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                console.log(age);
                if(age >= 17){
                    $("#status-ektpdetail").show();
                    $("#status_ektpdetailvalue").val(data.data.status_ektp);

                }

        $("#dataAgama").val(data.data.agama).change();
        $("#dataPendidikan").val(data.data.pendidikan).change();
        $("#dataPekerjaan").val(data.data.jenis_pekerjaan).change();
        $("#dataGolonganDarah").val(data.data.golongan_darah).change();
        console.log(data.data.status_perkawinan);
        $("#statusPerkawinanDetail").val(data.data.status_perkawinan);
        if(data.data.status_perkawinan == 'kawin_tercatat'){
            $("#tanggalPerkawinanEdit").show();
            $("#no_akta_perkawinanEdit").show();
            $("#tanggalPerceraianEdit").hide();
            $("#tanggal_perkawinanedit").val(data.data.tanggal_perkawinan);
            $("#no_akta_pernikahanedit").val(data.data.no_akta_pernikahan);

        }
        else if(data.data.status_perkawinan == 'kawin_belum_tercatat'){
            $("#tanggalPerkawinanEdit").show();
            $("#no_akta_perkawinanEdit").hide();
            $("#tanggalPerceraianEdit").hide();
            $("#tanggal_perkawinanedit").val(data.data.tanggal_perkawinan);
            $("#no_akta_pernikahanedit").val(data.data.no_akta_pernikahan);

        }
        else if(data.data.status_perkawinan == 'cerai_hidup' && data.data.status_perkawinan == 'cerai_mati'){
            $("#tanggalPerkawinan").hide();
            $("#tanggalPerceraian").show();
        }else{
            $("#tanggalPerkawinan").hide();
            $("#tanggalPerceraian").hide();
        }
        $("#statusDidalamKeluarga").val(data.data.status_didalam_keluarga);

        $("#textNamaLengkap").html(data.data.nama_lengkap);
        $("#textNamaPanggilan").html(data.data.nama_panggilan); //

        if(data.data.ayah != null){
            $("#nama_ayah").html(data.data.ayah.nama_lengkap); //
            $("#nama_ayah").html(""); //
        }

        if(data.data.ibu != null){
            $("#nama_ibu").html(data.data.ibu.nama_lengkap); //
        }else{
            $("#nama_ibu").html(""); //

        }

        $("#no_akta_kelahiranedit").val(data.data.no_akta)
        $("#desa_detail").val(data.data.desa)
        $("#kelurahan_detail").val(data.data.kelurahan)



      })
}

function tambahWarga(){
    $("#modalWarga").modal("show");
    $("#id").val("");
    $('#wargaForm').trigger("reset");
    $('#modelHeading').html("Form Warga");

}

$('#rolesForm').keypress(function(e) {
    if (e.which == '13') {
        console.log("masuk sini");
        submitForm();
    }
});


</script>
@endsection