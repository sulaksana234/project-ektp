@extends('layouts.backend.index')

@section('content')
<div class="page-body">
    <div class="row">
        <div class="col-md-12">
          
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Ajax data source (Arrays) table start -->
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>Data Provinsi</h2>
                                    </div>
                                    @if(Auth::user()->hasRole('superadmin'))
                                    <div class="col-md-6" style="text-align: right;">
                                        <button class="btn btn-primary btn-sm" onclick="tambahProvinsi()"><i class="fa fa-plus-circle"></i> Tambah Provinsi</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="table-responsive dt-responsive">
                                    <table id="provinsi" class="table table-striped table-bordered nowrap">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th>Action.</th>
                                             
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal blowup" id="modalProvinsi" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Form Permissions</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
            </div>
            <div class="modal-body">
                <form id="provinsiForm" name="provinsiForm" class="form-horizontal">
                    <input type="hidden" name="id" id="id">
                     <div class="form-group">
                         <label for="title" class="col-sm-4 control-label">Code</label>
                         <div class="col-sm-12">
                             <input type="text" class="form-control" id="code" name="code" placeholder="Code..." value="">
                         </div>
                     </div>
                     <div class="form-group">
                        <label for="display_name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Display Name" value="" >
                        </div>
                    </div>
      
       
                     <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-primary waves-effect waves-light" id="saveProvinsi">Save changes</button>
                     </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        fetchDataProvinsi();
        
    });

    $(document).keydown(function(event){
        console.log(event.which);
     
    });


    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveProvinsi').click(function (e) {
            e.preventDefault();
                $(this).html('<img src="/img/elipseloading.gif"></img>');
                $.ajax({
                data: $('#provinsiForm').serialize(),
                url: "{{ route('store.province') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.success){
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'success'
                        });
                        $("#modalProvinsi").modal("hide");
                        $('#provinsiForm').trigger("reset");
                        $('#saveProvinsi').html('Save Changes');

                        fetchDataProvinsi();

                    }else{
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'error'
                        });
                        $('#saveProvinsi').html('Save Changes');

                        fetchDataProvinsi();


                    }
                  
                
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#saveProvinsi').html('Save Changes');
                }
            });
        });
    });

function deleteProvinsi(value){
    Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Kamu akan menghapus data provinsi ini !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `/province/delete/`+value,
                type: "DELETE",
                cache: false,
           
                success:function(response){ 

                    Swal.fire({
                        type: 'success',
                        icon: 'success',
                        title: `${response.message}`,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    fetchDataProvinsi();

                }
            });
        }
    })
}

function editProvinsi(value){
    $("#id").val(value);
    $.get("province/edit/" + value , function (data) {
          $('#modelHeading').html("Edit Permissions");
          $('#saveProvinsi').val("edit-provinsi");
          $("#modalProvinsi").modal("show");
          $('#code').val(data.code);
          $('#name').val(data.name);

      })


}

function fetchDataProvinsi(){
    $('#provinsi').DataTable().destroy();

    $('#provinsi').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/province/data",
        "columns":[
            {"data": 'DT_RowIndex', "name": 'DT_RowIndex'},
            {"data": 'code', "name": 'code'},
            {"data": 'name', "name": 'name'},
            {"data": 'action', "name": 'action'},

        ],
    }).draw();
}

function submitForm(){
            $('#savePermissions').html('<img src="/img/elipseloading.gif"></img>');
            $.ajax({
                data: $('#permissionsForm').serialize(),
                url: "{{ route('store.permissions') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.success){
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'success'
                        });
                        $("#modalPermissions").modal("hide");
                        $('#permissionsForm').trigger("reset");
                        $('#savePermissions').html('Save Changes');
                        fetchDataPermission();

                    }else{
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'error'
                        });
                        $('#savePermissions').html('Save Changes');
                        fetchDataPermission();


                    }
                  
                
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#savePermissions').html('Save Changes');
                }
            });
}

function tambahProvinsi(){
    $("#modalProvinsi").modal("show");
    $("#id").val("");
    $('#provinsiForm').trigger("reset");
    $('#modelHeading').html("Form Provinsi");

}

$('#provinsiForm').keypress(function(e) {
    if (e.which == '13') {
        submitForm();
    }
});


</script>
@endsection