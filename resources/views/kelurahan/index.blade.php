@extends('layouts.backend.index')

@section('content')
<div class="page-body">
    <div class="row">
        <div class="col-md-12">
          
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Ajax data source (Arrays) table start -->
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>Data Kelurahan</h2>
                                    </div>
                                    @if(Auth::user()->hasRole('superadmin'))
                                    <div class="col-md-6" style="text-align: right;">
                                        <button class="btn btn-primary btn-sm" onclick="tambahKelurahan()"><i class="fa fa-plus-circle"></i> Tambah Kelurahan</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="table-responsive dt-responsive">
                                    <table id="kelurahan" class="table table-striped table-bordered nowrap">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th>Action.</th>
                                             
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal blowup" id="modalKelurahan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Form Kelurahan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
            </div>
            <div class="modal-body">
                <form id="kelurahanForm" name="kelurahanForm" class="form-horizontal">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="title" class="col-sm-4 control-label">Kecamatan</label>
                        <div class="col-sm-12">
                            <select name="district_code" id="district_code" class="form-control select2">
                                @foreach($kecamatan as $kecamatan)
                                <option value="{{$kecamatan->code}}">{{$kecamatan->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                     <div class="form-group">
                         <label for="title" class="col-sm-4 control-label">Code</label>
                         <div class="col-sm-12">
                             <input type="text" class="form-control" id="code" name="code" placeholder="Code..." value="">
                         </div>
                     </div>
                     <div class="form-group">
                        <label for="display_name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Display Name" value="" >
                        </div>
                    </div>
      
       
                     <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-primary waves-effect waves-light" id="saveKelurahan">Save changes</button>
                     </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        fetchKelurahan();
        
    });

    $(document).keydown(function(event){
        console.log(event.which);
     
    });


    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveKelurahan').click(function (e) {
            e.preventDefault();
                $(this).html('<img src="/img/elipseloading.gif"></img>');
                $.ajax({
                data: $('#kelurahanForm').serialize(),
                url: "{{ route('store.kelurahan') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.success){
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'success'
                        });
                        $("#modalKelurahan").modal("hide");
                        $('#kelurahanForm').trigger("reset");
                        $('#saveKelurahan').html('Save Changes');

                        fetchKelurahan();

                    }else{
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'error'
                        });
                        $('#saveKelurahan').html('Save Changes');

                        fetchKelurahan();


                    }
                  
                
                },
                error: function (data) {
                    $('#saveKelurahan').html('Save Changes');
                }
            });
        });
    });

function deleteKelurahan(value){
    Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Kamu akan menghapus data kelurahan ini !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `/kelurahan/delete/`+value,
                type: "DELETE",
                cache: false,
           
                success:function(response){ 

                    Swal.fire({
                        type: 'success',
                        icon: 'success',
                        title: `${response.message}`,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    fetchKelurahan();

                }
            });
        }
    })
}

function editKelurahan(value){
    $("#id").val(value);
    $.get("kelurahan/edit/" + value , function (data) {
            console.log
          $('#modelHeading').html("Edit Kelurahan");
          $('#saveKelurahan').val("edit-kelurahan");
          $("#modalKelurahan").modal("show");
          $('#district_code').val(data.district_code);

          $('#code').val(data.code);
          $('#name').val(data.name);

      })


}

function fetchKelurahan(){
    $('#kelurahan').DataTable().destroy();

    $('#kelurahan').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/kelurahan/data",
        "columns":[
            {"data": 'DT_RowIndex', "name": 'DT_RowIndex'},
            {"data": 'code', "name": 'code'},
            {"data": 'name', "name": 'name'},
            {"data": 'action', "name": 'action'},

        ],
    }).draw();
}

function submitForm(){
            $('#saveKelurahan').html('<img src="/img/elipseloading.gif"></img>');
            $.ajax({
                data: $('#kelurahanForm').serialize(),
                url: "{{ route('store.kelurahan') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.success){
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'success'
                        });
                        $("#modalKelurahan").modal("hide");
                        $('#kelurahanForm').trigger("reset");
                        $('#saveKelurahan').html('Save Changes');
                        fetchDataKelurahan();

                    }else{
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'error'
                        });
                        $('#saveKelurahan').html('Save Changes');
                        fetchDataKelurahan();


                    }
                  
                
                },
                error: function (data) {
                    $('#saveKelurahan').html('Save Changes');
                }
            });
}

function tambahKelurahan(){
    $("#modalKelurahan").modal("show");
    $("#id").val("");
    $('#kelurahanForm').trigger("reset");
    $('#modelHeading').html("Form Kelurahan");

}

$('#kelurahanForm').keypress(function(e) {
    if (e.which == '13') {
        submitForm();
    }
});


</script>
@endsection