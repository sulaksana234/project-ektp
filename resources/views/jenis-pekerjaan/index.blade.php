@extends('layouts.backend.index')

@section('content')
<div class="page-body">
    <div class="row">
        <div class="col-md-12">
          
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Ajax data source (Arrays) table start -->
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>Data Jenis Pekerjaan</h2>
                                    </div>
                                    <div class="col-md-6" style="text-align: right;">
                                        <button class="btn btn-primary btn-sm" onclick="tambahJenisPekerjaan()"><i class="fa fa-plus-circle"></i> Tambah Jenis Pekerjaan</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="table-responsive dt-responsive">
                                    <table id="jenisPekerjaan" class="table table-striped table-bordered nowrap">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Display Name</th>
                                                <th>Action.</th>
                                             
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalJenisPekerjaan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Form Jenis Pekerjaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
            </div>
            <div class="modal-body">
                <form id="jenisPekerjaanForm" name="jenisPekerjaanForm" class="form-horizontal">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="title" class="col-sm-4 control-label">Nama</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name..." value="">
                        </div>  
                    </div>
                     <div class="form-group">
                         <label for="display_name" class="col-sm-4 control-label">Display Name</label>
                         <div class="col-sm-12">
                             <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Display Name..." value="">
                         </div>
                     </div>
             
      
       
                     <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-primary waves-effect waves-light" id="saveJenisPekerjaan">Save changes</button>
                     </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        fetchDataJenisPekerjaan();
        
    });

    $(document).keydown(function(event){
        console.log(event.which);
     
    });


    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveJenisPekerjaan').click(function (e) {
            e.preventDefault();
                $(this).html('<img src="/img/elipseloading.gif"></img>');
                $.ajax({
                data: $('#jenisPekerjaanForm').serialize(),
                url: "{{ route('store.jenisPekerjaan') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.success){
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'success'
                        });
                        $("#modalJenisPekerjaan").modal("hide");
                        $('#jenisPekerjaanForm').trigger("reset");
                        $('#saveJenisPekerjaan').html('Save Changes');

                        fetchDataJenisPekerjaan();

                    }else{
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'error'
                        });
                        $('#saveJenisPekerjaan').html('Save Changes');

                        fetchDataJenisPekerjaan();


                    }
                  
                
                },
                error: function (data) {
                    $('#saveJenisPekerjaan').html('Save Changes');
                }
            });
        });
    });

function deleteJenisPekerjaan(value){
    Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Kamu akan menghapus data ini !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `/jenisPekerjaan/delete/`+value,
                type: "DELETE",
                cache: false,
           
                success:function(response){ 

                    Swal.fire({
                        type: 'success',
                        icon: 'success',
                        title: `${response.message}`,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    fetchDataJenisPekerjaan();

                }
            });
        }
    })
}

function editJenisPekerjaan(value){
    $("#id").val(value);
    $.get("jenisPekerjaan/edit/" + value , function (data) {
            console.log
          $('#modelHeading').html("Edit Kabupaten");
          $('#saveJenisPekerjaan').val("edit-kabupaten");
          $("#modalJenisPekerjaan").modal("show");

          $('#name').val(data.name);
          $('#display_name').val(data.display_name);
      })


}

function fetchDataJenisPekerjaan(){
    $('#jenisPekerjaan').DataTable().destroy();

    $('#jenisPekerjaan').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/jenisPekerjaan/data",
        "columns":[
            {"data": 'DT_RowIndex', "name": 'DT_RowIndex'},
            {"data": 'name', "name": 'name'},
            {"data": 'display_name', "name": 'display_name'},
            {"data": 'action', "name": 'action'},

        ],
    }).draw();
}

function submitForm(){
    $("#saveJenisPekerjaan").html('<img src="/img/elipseloading.gif"></img>');
                $.ajax({
                data: $('#jenisPekerjaanForm').serialize(),
                url: "{{ route('store.jenisPekerjaan') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.success){
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'success'
                        });
                        $("#modalJenisPekerjaan").modal("hide");
                        $('#jenisPekerjaanForm').trigger("reset");
                        $('#saveJenisPekerjaan').html('Save Changes');

                        fetchDataJenisPekerjaan();

                    }else{
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'error'
                        });
                        $('#saveJenisPekerjaan').html('Save Changes');

                        fetchDataJenisPekerjaan();


                    }
                  
                
                },
                error: function (data) {
                    $('#saveJenisPekerjaan').html('Save Changes');
                }
            });
}

function tambahJenisPekerjaan(){
    $("#modalJenisPekerjaan").modal("show");
    $("#id").val("");
    $('#jenisPekerjaanForm').trigger("reset");
    $('#modelHeading').html("Form Jenis Pekerjaan");

}

$('#jenisPekerjaanForm').keypress(function(e) {
    if (e.which == '13') {
        submitForm();
    }
});


</script>
@endsection