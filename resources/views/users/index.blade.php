@extends('layouts.backend.index')

@section('content')
<div class="page-body">
    <div class="row">
        <div class="col-md-12">
          
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Ajax data source (Arrays) table start -->
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>Data Users</h2>
                                    </div>
                                    <div class="col-md-6" style="text-align: right;">
                                        <button class="btn btn-primary btn-sm" onclick="tambahUsers()"><i class="fa fa-plus-circle"></i> <code data-intro="Tambah Users" data-step="1" data-hint="Button Untuk Tambah User">Tambah User</code></button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="table-responsive dt-responsive">
                                    <table id="users" class="table table-striped table-bordered nowrap">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Verification</th>
                                                <th>Roles</th>
                                                <th>Action.</th>
                                             
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalUsers" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Form User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
            </div>
            <div class="modal-body">
                <form id="userForm" name="userForm" class="form-horizontal">
                    <input type="hidden" name="id" id="id">
                     <div class="form-group">
                         <label for="name" class="col-sm-4 control-label">Name</label>
                         <div class="col-sm-12">
                             <input type="text" class="form-control" id="name" name="name" placeholder="Nama Lengkap" value="" required>
                         </div>
                     </div>
                     <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-12">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="" required>
                        </div>
                    </div>
      
                     <div class="form-group" id="passwordFormGroup">
                         <label class="col-sm-4 control-label">Password</label>
                         <div class="col-sm-12">
                             <input id="password" name="password" required placeholder="Enter Description" class="form-control" type="password">
                         </div>
                     </div>
       
                     <div class="form-group">
                        <label class="col-sm-4 control-label">Roles</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="roles[]" id="roles" style="width:100%;" multiple="true">
                                <option value="null"> == Pilih Roles User == </option>
                                @foreach($role as $role)
                                    <option value="{{$role->id}}"> {{$role->display_name}} </option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                     <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-primary waves-effect waves-light" id="saveUser"><i class="fa fa-save"></i> Save changes</button>
                     </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
<script>
    var arrayRolesSelected = [];
    $(document).ready(function(){
        fetchDataUser();
        
    });


    $(document).keydown(function(event){
        // if(event.ctrlKey && event.keyCode == nCode) {
        //     tambahUsers();
        // }
    });


    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveUser').click(function (e) {
            e.preventDefault();
            console.log("masuk sini");
            $(this).html('<img src="/img/elipseloading.gif"></img>');
                $.ajax({
                    data: $('#userForm').serialize(),
                    url: "{{ route('store.users') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        if(data.success){
                            new PNotify({
                                title: data.message,
                                text: data.message,
                                icon: 'icofont icofont-info-circle',
                                type: 'success'
                            });
                            $("#modalUsers").modal("hide");
                            $('#userForm').trigger("reset");
                            $('#saveUser').html('Save Changes');

                            fetchDataUser();

                        }else{
                            new PNotify({
                                title: data.message,
                                text: data.message,
                                icon: 'icofont icofont-info-circle',
                                type: 'error'
                            });
                            $('#saveUser').html('Save Changes');

                            fetchDataUser();


                        }
                    
                    
                    },
                    error: function (data) {
                        $('#saveUser').html('Save Changes');
                    }
            });
        });
    });

function deleteUser(value){
    Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Kamu akan menghapus data user ini !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `/users/delete/`+value,
                type: "DELETE",
                cache: false,
           
                success:function(response){ 

                    //show success message
                    Swal.fire({
                        type: 'success',
                        icon: 'success',
                        title: `${response.message}`,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    fetchDataUser();

                    //remove post on table
                }
            });
        }
    })
}

function editUser(value){
    arrayRolesSelected = [];
    $("#id").val(value);
    $.get("users/edit/" + value , function (data) {
          $('#modelHeading').html("Edit User");
          $('#saveRoles').val("edit-users");
          $("#modalUsers").modal("show");
          $("#passwordFormGroup").hide();

          $('#name').val(data.name);
          $('#email').val(data.email);
          $.each(data.roles, function(item,value){
            arrayRolesSelected.push(value.id)
          });

          $('#roles').val(arrayRolesSelected).change();

      })


}

function fetchDataUser(){
    $('#users').DataTable().destroy();

    $('#users').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/users/data",
        "columns":[
            {"data": 'DT_RowIndex', "name": 'DT_RowIndex'},
            {"data": 'name', "name": 'name'},
            {"data": 'email', "name": 'email'},
            {"data": 'email_verified_at',render: function(data,index,row){
                console.log(data,index,row); 
                if(row.email_verified_at != null){
                    return '<button class="btn btn-primary btn-sm editProduct"><i class="fa fa-check"></i> Verification</button>';
                }else{
                    return '<button class="btn btn-danger btn-sm editProduct"><i class="fa fa-times"></i> Underverifikation</button>';
                }
            }},
            {"data": 'roles', render: function(data,index,row){
                return '<span class="badge badge-primary">'+row.roles.replace(/&lt;br&gt;/g, ' , ')+'</span>';
            }},
            {"data": 'action', "name": 'action'},

        ],
    }).draw();
}

function submitForm(){
            $('#saveRoles').html('<img src="/img/elipseloading.gif"></img>');
            $.ajax({
                data: $('#rolesForm').serialize(),
                url: "{{ route('store.roles') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.success){
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'success'
                        });
                        $("#modalRoles").modal("hide");
                        $('#rolesForm').trigger("reset");
                        $('#saveRoles').html('Save Changes');
                        fetchDataRoles();

                    }else{
                        new PNotify({
                            title: data.message,
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'error'
                        });
                        $('#saveRoles').html('Save Changes');
                        fetchDataRoles();


                    }
                  
                
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#saveRoles').html('Save Changes');
                }
            });
}

function tambahUsers(){
    $("#modalUsers").modal("show");
    $("#id").val("");
    $('#rolesForm').trigger("reset");
    $('#modelHeading').html("Form Roles");

}

$('#rolesForm').keypress(function(e) {
    if (e.which == '13') {
        console.log("masuk sini");
        submitForm();
    }
});


</script>
@endsection