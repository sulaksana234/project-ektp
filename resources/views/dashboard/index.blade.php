@extends('layouts.backend.index')

@section('content')
@php
    $date = \Carbon\Carbon::now();
@endphp
<div class="page-body">
    <div class="row">
        <!-- task, page, download counter  start -->
        <div class="col-md-12">
            <div class="card bg-c-yellow" style="background-color:#FCAE1E;">
                <div class="card-body row">
                    <div class="col-md-6">
                        <img src="/template/assets/images/logo.png" style="height: 250px;width:50%;" alt="">
                    </div>
                    <div class="col-md-6">
                        <span style="color:white;font-weight: bold;font-size:40px;">Hello {{Auth::user()->name}}</span><br>
                        <span style="color:white;font-weight: bold;font-size:20px;">Selamat Datang Di</span><br>
                        <span style="color:white;font-weight: bold;font-size:20px;">SISTEM PENGELOLAAN PENDUDUK (SIPP) LINGKUNGAN KERTHA PASCIMA </span><br>
                        <span style="color:white;font-weight: bold;font-size:20px;">KELURAHAN TANJUNG BENOA</span>
                    </div>
              
                </div>
            </div>
        </div>
        @if ("{{Auth::user()->hasRole('administrator')}}")
        <div class="col-md-12 mb-2">
            <label for="">Pilih Data Banjar :</label>
            <select name="banjar" id="banjar" class="form-control">
                <option value="kertha_pascima">BR.Kertha Pascima</option>
                <option value="purwa_santhi">BR.Purwa Santhi</option>
                <option value="madya_utarayana">BR.Madya Utarayana</option>
                <option value="daksinayana">BR.Daksinayana</option>
            </select>
        </div>
        @endif
        <div class="col-xl-3 col-md-6">
            <div class="card bg-c-yellow update-card">
                <div class="card-block">
                    <div class="row align-items-end">
                        <div class="col-8">
                            <h4 class="text-white" id="jumlahWarga">{{$jumlahWarga}}</h4>
                            <h6 class="text-white m-b-0">Jumlah Warga<button type="button" class="btn btn-sm btn-danger" data-toggle="popover" title="Perbandingan Jumlah Warga" data-content="Perbandingan jumlah warga berdasarkan gender laki atau perempuan"><i class="fa fa-info-circle" aria-hidden="true"></i></button></h6>
                        </div>
                        <div class="col-4 text-right">
                            <canvas id="update-chart-1" height="50"></canvas>
                        </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg" id="jumlahWargaModal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" style="color:black;">Data Warga Pendatang dan Gegem</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
                                </div>
                                <div class="modal-body">
                                    <span style="color: red;font-style:italic">Catatan: Data Warga Pendatang dan Gegem yang dihitung berdasarkan Gender</span>
                                   <div class="row">
                                    <div class="col-xl-6 col-md-6">
                                        <div class="card bg-c-yellow update-card">
                                            <div class="card-block">
                                                <div class="row align-items-end">
                                                    <div class="col-8">
                                                        <h4 class="text-white" id="jumlahWarga">{{$jumlahWargaPriaPendatang}}</h4>
                                                        <h6 class="text-white m-b-0">Jumlah Pendatang Pria</h6>
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <canvas id="update-chart-1" height="50"></canvas>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="card-footer">
                                                <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6">
                                        <div class="card bg-c-yellow update-card">
                                            <div class="card-block">
                                                <div class="row align-items-end">
                                                    <div class="col-8">
                                                        <h4 class="text-white" id="jumlahWarga">{{$jumlahWargaPria}}</h4>
                                                        <h6 class="text-white m-b-0">Jumlah Pria</h6>
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <canvas id="update-chart-1" height="50"></canvas>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="card-footer">
                                                <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6">
                                        <div class="card bg-c-yellow update-card">
                                            <div class="card-block">
                                                <div class="row align-items-end">
                                                    <div class="col-8">
                                                        <h4 class="text-white" id="jumlahWarga">{{$jumlahWargaWanitaPendatang}}</h4>
                                                        <h6 class="text-white m-b-0">Jumlah Pendatang Wanita</h6>
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <canvas id="update-chart-1" height="50"></canvas>
                                                    </div>
                                                </div>
                                      
                                            </div>
                                            <div class="card-footer">
                                                <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6">
                                        <div class="card bg-c-yellow update-card">
                                            <div class="card-block">
                                                <div class="row align-items-end">
                                                    <div class="col-8">
                                                        <h4 class="text-white" id="jumlahWarga">{{$jumlahWargaWanita}}</h4>
                                                        <h6 class="text-white m-b-0">Jumlah Wanita</h6>
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <canvas id="update-chart-1" height="50"></canvas>
                                                    </div>
                                                </div>
                                      
                                            </div>
                                            <div class="card-footer">
                                                <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                   </div>
                                </div>
                           
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }}</p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-c-yellow update-card">
                <div class="card-block">
                    <div class="row align-items-end">
                        <div class="col-8">
                            <h4 class="text-white" id="jumlahPersentaseEktp">{{$persentasiJumlahPemilikEktp}}  %</h4>
                            <h6 class="text-white m-b-0">E-KTP<button type="button" class="btn btn-sm btn-danger" data-toggle="popover" title="Perbandingan Warga yang telah memiliki E-KTP" data-content="Perbandingan Warga Yang Telah Memiliki E-KTP atau belum memiliki E-KTP"><i class="fa fa-info-circle" aria-hidden="true"></i></button></h6>
                            <div class="modal fade bd-example-modal-lg" id="jumlahPersentaseWargaYangTelahMemilikidanbelumEktp" tabindex="-1" role="dialog">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" style="color:black;">Data Persentase Warga Pemilik E-KTP</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                                        </div>
                                        <div class="modal-body">
                                            <span style="color: red;font-style:italic">Catatan: Data persentase Warga Yang Telah memiliki E-KTP Serta Belum memiiki E-KTP dan juga belum melakukan pengajuan E-KTP</span>
                                           <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="card bg-c-yellow update-card">
                                                    <div class="card-block">
                                                        <div class="row align-items-end">
                                                            <div class="col-8">
                                                                <h4 class="text-white" id="jumlahWarga">{{$persentasiJumlahPengajuanEktp}} %</h4>
                                                                <h6 class="text-white m-b-0">Persentase warga yang sudah melakukan pengajuan e-ktp</h6>
                                                            </div>
                                                            <div class="col-4 text-right">
                                                                <canvas id="update-chart-1" height="50"></canvas>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="card bg-c-yellow update-card">
                                                    <div class="card-block">
                                                        <div class="row align-items-end">
                                                            <div class="col-8">
                                                                <h4 class="text-white" id="jumlahWarga">{{$persentasiJumlahBelumMempunyaiEktp}}</h4>
                                                                <h6 class="text-white m-b-0">Persentase warga yang belum melakukan pengajuan e-kt</h6>
                                                            </div>
                                                            <div class="col-4 text-right">
                                                                <canvas id="update-chart-1" height="50"></canvas>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                     
                                           </div>
                                        </div>
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 text-right">
                            <canvas id="update-chart-2" height="50"></canvas>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }}</p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-c-yellow update-card">
                <div class="card-block">
                    <div class="row align-items-end">
                        <div class="col-8">
                            <h4 class="text-white" id="jumlahKk">{{count($jumlahKK)}}</h4>
                            <h6 class="text-white m-b-0">Jumlah KK<button type="button" class="btn btn-sm btn-danger" data-toggle="popover" title="Data jumlah Kepala Keluarga" data-content="Data Jumlah Kepala Keluarga"><i class="fa fa-info-circle" aria-hidden="true"></i></button></h6>
                        </div>
                        <div class="col-4 text-right">
                            <canvas id="update-chart-3" height="50"></canvas>
                        </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg" id="perbandinganKkPendatangDenganGegem" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" style="color:black;">Jumlah Kepala Keluarga</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
                                </div>
                                <div class="modal-body">
                                    <span style="color:red;font-style:italic;">Perbadingan Jumlah KK Gegem Dengan Pendatang</span><br>

                                   <div class="row">
                                    <div class="col-xl-6 col-md-6">
                                        <div class="card bg-c-yellow update-card">
                                            <div class="card-block">
                                                <div class="row align-items-end">
                                                    <div class="col-8">
                                                        <h5 class="text-white" id="jumlahWarga"> Jumlah KK Tanjung Benoa : {{count($jumlahKKGegem)}}</h5>
                                                        <small class="text-white" id="jumlahWargaPertempekan">Jumlah Warga Tempekan 1 : {{count($jumlahKKGegemYudistira)}}</small>
                                                        <small class="text-white" id="jumlahWargaPertempekan">Jumlah Warga Tempekan 2 : {{count($jumlahKKGegemBima)}}</small>
                                                        <small class="text-white" id="jumlahWargaPertempekan">Jumlah Warga Tempekan 3 : {{count($jumlahKKGegemArjuna)}}</small>
                                                        <small class="text-white" id="jumlahWargaPertempekan">Jumlah Warga Tempekan Nakula & Sahadewa : {{count($jumlahKKGegemNakulaSahadewa)}}</small>
                                                        <h6 class="text-white m-b-0"></h6>
                                                       
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <canvas id="update-chart-1" height="50"></canvas>
                                                    </div>
                                                </div>
                                          
                                            </div>
                                            <div class="card-footer">
                                                <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6">
                                        <div class="card bg-c-yellow update-card">
                                            <div class="card-block">
                                                <div class="row align-items-end">
                                                    <div class="col-8">
                                                        <h4 class="text-white" id="jumlahWarga">{{count($jumlahKKPendatang)}}</h4>
                                                        <h6 class="text-white m-b-0">Jumlah KK Pendatang</h6>
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <canvas id="update-chart-1" height="50"></canvas>
                                                    </div>
                                                </div>
                                                <div class="modal fade modal-flex" id="jumlahWargaModal" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" style="color:black;">Detail Jumlah Warga</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h5>Static Modal</h5>
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                   </div>
                                </div>
                           
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }}</p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-c-yellow update-card">
                <div class="card-block">
                    <div class="row align-items-end">
                        <div class="col-8">
                            <h4 class="text-white">500</h4>
                            <h6 class="text-white m-b-0">Downloads</h6>
                        </div>
                        <div class="col-4 text-right">
                            <canvas id="update-chart-4" height="50"></canvas>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : {{ $date->format('Y-m-d H:i:s') }}</p>
                </div>
            </div>
        </div>
        <!-- task, page, download counter  end -->

        <!--  sale analytics start -->
        {{-- <div class="col-xl-9 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Sales Analytics</h5>
                    <span class="text-muted">For more details about usage, please refer <a href="https://www.amcharts.com/online-store/" target="_blank">amCharts</a> licences.</span>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li><i class="feather icon-maximize full-card"></i></li>
                            <li><i class="feather icon-minus minimize-card"></i></li>
                            <li><i class="feather icon-trash-2 close-card"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-block">
                    <div id="sales-analytics" style="height: 265px;"></div>
                </div>
            </div>
        </div> --}}
        {{-- <div class="col-xl-3 col-md-12">
            <div class="card user-card2">
                <div class="card-block text-center">
                    <h6 class="m-b-15">Project Risk</h6>
                    <div class="risk-rate">
                        <span><b>5</b></span>
                    </div>
                    <h6 class="m-b-10 m-t-10">Balanced</h6>
                    <a href="#!" class="text-c-yellow b-b-warning">Change Your Risk</a>
                    <div class="row justify-content-center m-t-10 b-t-default m-l-0 m-r-0">
                        <div class="col m-t-15 b-r-default">
                            <h6 class="text-muted">Nr</h6>
                            <h6>AWS 2455</h6>
                        </div>
                        <div class="col m-t-15">
                            <h6 class="text-muted">Created</h6>
                            <h6>30th Sep</h6>
                        </div>
                    </div>
                </div>
                <button class="btn btn-warning btn-block p-t-15 p-b-15">Download Overall Report</button>
            </div>
        </div> --}}
        <!--  sale analytics end -->

        <div class="col-xl-8 col-md-12">
            <div class="card table-card">
                <div class="card-header bg-c-yellow">
                    <h5 style="color: white;font-weight: bold;">List Warga Belum Mendapatkan E-KTP</h5>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li><i class="feather icon-maximize full-card"></i></li>
                            <li><i class="feather icon-minus minimize-card"></i></li>
                            <li><i class="feather icon-trash-2 close-card"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive dt-responsive" id="dataEktpTable" style="padding:10px;">
                                <table id="tableDataEktp" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>NAMA</th>
                                            <th>STATUS E-KTP</th>
                                         
        
                                            <th>Action.</th>
                                         
                                        </tr>
                                    </thead>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
               
                </div>
            </div>
        </div>
        {{-- <div class="col-xl-4 col-md-12">
            <div class="card user-activity-card">
                <div class="card-header">
                    <h5>User Activity</h5>
                </div>
                <div class="card-block">
                    <div class="row m-b-25">
                        <div class="col-auto p-r-0">
                            <div class="u-img">
                                <img src="..\files\assets\images\breadcrumb-bg.jpg" alt="user image" class="img-radius cover-img">
                                <img src="..\files\assets\images\avatar-2.jpg" alt="user image" class="img-radius profile-img">
                            </div>
                        </div>
                        <div class="col">
                            <h6 class="m-b-5">John Deo</h6>
                            <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                            <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                        </div>
                    </div>
                    <div class="row m-b-25">
                        <div class="col-auto p-r-0">
                            <div class="u-img">
                                <img src="..\files\assets\images\breadcrumb-bg.jpg" alt="user image" class="img-radius cover-img">
                                <img src="..\files\assets\images\avatar-2.jpg" alt="user image" class="img-radius profile-img">
                            </div>
                        </div>
                        <div class="col">
                            <h6 class="m-b-5">John Deo</h6>
                            <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                            <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                        </div>
                    </div>
                    <div class="row m-b-25">
                        <div class="col-auto p-r-0">
                            <div class="u-img">
                                <img src="..\files\assets\images\breadcrumb-bg.jpg" alt="user image" class="img-radius cover-img">
                                <img src="..\files\assets\images\avatar-2.jpg" alt="user image" class="img-radius profile-img">
                            </div>
                        </div>
                        <div class="col">
                            <h6 class="m-b-5">John Deo</h6>
                            <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                            <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                        </div>
                    </div>
                    <div class="row m-b-5">
                        <div class="col-auto p-r-0">
                            <div class="u-img">
                                <img src="..\files\assets\images\breadcrumb-bg.jpg" alt="user image" class="img-radius cover-img">
                                <img src="..\files\assets\images\avatar-2.jpg" alt="user image" class="img-radius profile-img">
                            </div>
                        </div>
                        <div class="col">
                            <h6 class="m-b-5">John Deo</h6>
                            <p class="text-muted m-b-0">Lorem Ipsum is simply dummy text.</p>
                            <p class="text-muted m-b-0"><i class="feather icon-clock m-r-10"></i>2 min ago</p>
                        </div>
                    </div>

                    <div class="text-center">
                        <a href="#!" class="b-b-primary text-primary">View all Projects</a>
                    </div>
                </div>
            </div>
        </div> --}}

      



    </div>

 
</div>

    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#jumlahWarga").mouseover(function(){
            $("#jumlahWargaModal").modal("show");    
        });

        $("#jumlahKk").mouseover(function(){
            $("#perbandinganKkPendatangDenganGegem").modal("show");    
        });

        $("#jumlahPersentaseEktp").mouseover(function(){
            $("#jumlahPersentaseWargaYangTelahMemilikidanbelumEktp").modal("show");
        });

        fetchDataEktp();
    });

    function fetchDataEktp(){
        $('#tableDataEktp').DataTable().destroy();
        $('#tableDataEktp').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/dashboard/dataektp",
            "columns":[
                
                {"data": 'DT_RowIndex', "name": 'DT_RowIndex'},
                {"data": 'nama_lengkap', "name": 'nama_lengkap'},
                {"data": 'status_ektp',render: function(data,row,value){
                    if(value.status_ektp == 'no_ektp'){
                        return '<span class="badge badge-pill badge-danger">Belum Memiliki E-KTP</span>';
                    }
                }},

            
                {"data": 'action', "name": 'action'},

            ],
        }).draw();

    }
</script>
@endsection