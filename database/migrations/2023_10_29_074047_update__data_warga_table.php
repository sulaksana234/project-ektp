<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::table('data_warga', function (Blueprint $table) {
            $table->string('status_perkawinan')->nullable();
            $table->date('tanggal_perkawinan')->nullable();
            $table->enum('status_didalam_keluarga',['kepala_keluarga','istri','anak'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
