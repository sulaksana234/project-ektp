<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::table('data_warga', function($table)
        {
            $table->enum('status_ektp',['no_ektp','pengajuan','proses','done']);
        });

        Schema::create('pengajuan_ektp', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('kartu_keluarga')->nullable();
            $table->string('ktp_elektronik')->nullable();
            $table->string('surat_keterangan_kehilangan_dari_kepolisian')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
