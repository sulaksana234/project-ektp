<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::table('data_warga', function (Blueprint $table) {
            $table->string('nik_ayah')->nullable();
            $table->string('nama_lengkap_ayah')->nullable();
            $table->string('nik_ibu')->nullable();
            $table->string('nama_lengkap_ibu')->nullable();
            $table->string('banjar')->nullable();
            $table->string('tempekan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
